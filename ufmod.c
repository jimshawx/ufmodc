#include <windows.h>
#include <assert.h>

#include "config.h"
#include "swab.h"
#include "diag.h"

#ifdef USE_DUMP
extern void diagnostics(uF_MOD_STATE *);
extern void debug(unsigned int);
extern void debugs(char *fmt, ...);
#endif

#ifdef INTEGRATION_TEST
extern void integration(void *, unsigned int);
#endif

#ifdef USE_TABLE_HZ
extern int pow2_table_AmigaPeriod(int);
extern int pow2_table_DoFlags(int);
#endif

#ifndef INLINE_X86
#include <math.h>
#endif

#ifdef USE_MEMCHECK
#include <crtdbg.h>
#endif

#include "ufmod.h"
#include "ufmodint.h"

static unsigned int rotate_right32(unsigned int value, unsigned int count)
{
	//const unsigned int mask = (CHAR_BIT * sizeof(value)) - 1;
	//count &= mask;
	//return (value >> count) | (value << (~count & mask));
	count &= 31;
	unsigned int i = value >> count;
	unsigned int j = value << (32 - count);
	return i | j;
}

static unsigned int rotate_left32(unsigned int value, unsigned int count)
{
	//const unsigned int mask = (CHAR_BIT * sizeof(value)) - 1;
	//count &= mask;
	//return (value << count) | (value >> (~count & mask));
	count &= 31;
	unsigned int i = value << count;
	unsigned int j = value >> (32 - count);
	return i | j;
}

static unsigned int shrd(unsigned int hi, unsigned int lo, unsigned int count)
{
	return (hi >> count) | (lo << (32 - count));
}

static unsigned int cdq(unsigned int value)
{
	return (value & 0x80000000) ? 0xffffffff : 0;
}

//static void assert(int i){}
//int mainCRTStartup(void) { 
//	uFMOD_PlaySong((void *)"tunes/1.xm", 0, XM_FILE);
//	Sleep(10000);
//	uFMOD_StopSong();
//	return 0;
//}

char _fltused = 1;

static void * __cdecl ufmemcpy(void *dst, const void *src, size_t size)
{
	unsigned char *csrc = (unsigned char *)src;
	unsigned char *cdst = dst;
	while (size--)
		*cdst++ = *csrc++;
	return dst;
}

static void * __cdecl ufmemset(void *dst, int value, size_t size)
{
	return memset(dst, value, size);
}

#pragma function(memset)
void * __cdecl memset(void *dst, int value, size_t size)
{
	unsigned char *cdst = dst;
	while (size--)
		*cdst++ = value;
	return dst;
}

//thanks to
//https://www.musicdsp.org/en/latest/Other/222-fast-exp-approximations.html
//ln(a^n) = n * ln(a)
//a=2, ln(2) = 0.693...
//2^n = e^(ln(2) * n)
//e^(i+f) = e^i * e^f (we can take i as the whole part of n, f as the fractional part)
//e^ln(2)*i * e^ln(2)*f
//2^i * e^(0.693 * f)
//(1<<i) * Taylor series of exp(x) for small x
static float pow2f(float x)
{
	int i = (int)x;
	x = (x - (float)i) * 0.69314718056f;//ln(2)
	if (i >= 0)
		return ((720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888f) * (1 << i);
	else
		return ((720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888f) * (1.0f / (1 << -i));
}

//unsigned int pcm[4] = { 0x20001, FSOUND_MixRate, FSOUND_MixRate * 4, 0x100004 };
static PCMWAVEFORMAT pcm =
{
	WAVE_FORMAT_PCM,
	2,//channels
	FSOUND_MixRate,
	FSOUND_MixRate * 4,
	(2*16)/8,//channels * bps / 8
	16//bps
};

void uFMOD_FreeSong(void);

static unsigned int __stdcall ufMOD_Thread(void *p);
static int LoadXM(void *lpXM);
static void uFMOD_SW_Fill(void);
static void Ramp(uF_MOD *module, int edi, unsigned int *mixbuff, unsigned int *mixbuffend);
static void DoFlags(uF_CHAN *channel, uF_INST *flags_iptr, uF_SAMP *flags_sptr);
static uF_PAT *DoNote(uF_MOD *module, uF_PAT *pattern);
static void DoEffs(uF_MOD *esi);

static void res_open(void *);
#define res_read mem_read
#define res_close mem_close
static void mem_read(char *eax_buff, unsigned int edx_readsize);
static void mem_open(void *);
static void mem_close(void);
static void file_open(void *);
static void file_read(char *eax_buff, unsigned int edx_readsize);
static void file_close(void);

static uF_MOD_STATE _mod = { 0 };

void uFMOD_Jump2Pattern(int pat)
{
	_mod.module.nextrow = 0;
	if (pat < _mod.module.numorders)
		_mod.module.nextorder = pat;
}

static const unsigned short vol_scale[] = {
	1, // -45 dB
	130, // -24
	164, // -23
	207, // -22
	260, // -21
	328, // -20
	413, // -19
	519, // -18
	654, // -17
	823, // -16
	1036, // -15
	1305, // -14
	1642, // -13
	2068, // -12
	2603, // -11
	3277, // -10
	4125, // -9
	5193, // -8
	6538, // -7
	8231, // -6
	10362, // -5
	13045, // -4
	16423, // -3
	20675, // -2
	26029, // -1
	32768 // 0 dB
};

void __stdcall uFMOD_SetVolume(unsigned int scale)
{
	if (scale <= 25)
		_mod.ufmod_vol = vol_scale[scale];
}

void __stdcall uFMOD_Pause(void)
{
	_mod.ufmod_pause = 1;
}

void __stdcall uFMOD_Resume(void)
{
	_mod.ufmod_pause = 0;
}

unsigned int __stdcall uFMOD_GetStats()
{
	unsigned int blk = _mod.RealBlock[0];
	return (unsigned int)_mod.tInfo[blk].R_vol | (_mod.tInfo[blk].L_vol << 16);
}

unsigned int __stdcall uFMOD_GetRowOrder()
{
	unsigned int blk = _mod.RealBlock[0];
	return (unsigned int)_mod.tInfo[blk].s_row | (_mod.tInfo[blk].s_order << 16);
}

unsigned int __stdcall uFMOD_GetTime(void)
{
	return _mod.time_ms;
}

static void *alloc(unsigned int size)
{
#ifdef USE_MEMCHECK
	return calloc(size, 1);
#else
	return HeapAlloc(_mod.hHeap, HEAP_ZERO_MEMORY | HEAP_NO_SERIALIZE, size);
#endif
}

static void mem_read(char *eax_buff, unsigned int edx_readsize)
{
	if (_mod.mmf_module_posn + edx_readsize  >= _mod.mmf_module_size)
		edx_readsize = _mod.mmf_module_size - _mod.mmf_module_posn;

//copy:
	if (edx_readsize > 0)
	{
		char *src = (unsigned char *)_mod.mmf_module_address + _mod.mmf_module_posn;
		_mod.mmf_module_posn += edx_readsize;
//mem_do_copy:
		while (edx_readsize--)
			*eax_buff++ = *src++;
	}
}

const char *__stdcall uFMOD_GetTitle(void)
{
	return _mod.szTtl;
}

static void mem_open(void *unused) { unused; }
static void mem_close(void) {}

static void res_open(void *pszName)
{
	HANDLE hm;
	HRSRC hr;
	DWORD size;
	HGLOBAL res;
	hm = GetModuleHandle(NULL);//won't work in a DLL
	hr = FindResource(_mod.hInstance, pszName, MAKEINTRESOURCE(RT_RCDATA));
	assert(hr != NULL);
	size = SizeofResource(hm, hr);
	res = LoadResource(hm, hr);
	_mod.mmf_module_size = size;
	_mod.mmf_module_posn = 0;
	_mod.mmf_module_resource = res;
}

static void file_open(void *esi_lpFileName)
{
	_mod.SW_Exit = CreateFile(esi_lpFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	_mod.mmf_module_size = GetFileSize(_mod.SW_Exit, NULL);
	_mod.mmf_module_posn = 0;

#ifdef USE_FILE_MAPPING
	//instead of using file, map it into ram and use memory access
	_mod.mmf_module_mapping = CreateFileMapping(_mod.SW_Exit, NULL, PAGE_READONLY, 0, 0, NULL);
	_mod.mmf_module_address = (char *)MapViewOfFile(_mod.mmf_module_mapping, FILE_MAP_READ, 0, 0, 0);
	_mod.uFMOD_fread = mem_read;
#endif
}

static int do_read(unsigned int ecx_offset, void *edi_buffer, unsigned int esi_size)
{
	int read=0;
	SetFilePointer(_mod.SW_Exit, ecx_offset, NULL, FILE_BEGIN);
	return ReadFile(_mod.SW_Exit, edi_buffer, esi_size, &read, NULL);
}

static void file_read(char *eax_buf, unsigned int edx_size)
{
	//remove all the cacheing
	do_read(_mod.mmf_module_posn, eax_buf, edx_size);

	_mod.mmf_module_posn += edx_size;
	if (_mod.mmf_module_posn > _mod.mmf_module_size)
		_mod.mmf_module_posn = _mod.mmf_module_size;
}

static void file_close(void)
{
#ifdef USE_FILE_MAPPING
	UnmapViewOfFile(_mod.mmf_module_address);
	CloseHandle(_mod.mmf_module_mapping);
#endif

	CloseHandle(_mod.SW_Exit);
}

#define UFMOD_LSEEK_SET 0
#define UFMOD_LSEEK_CUR 1
static void uFMOD_lseek(int eax_pos, int ecx_org, int Z_ufmod_lseek)
{
	if (Z_ufmod_lseek == UFMOD_LSEEK_CUR)
		eax_pos += _mod.mmf_module_posn;
	if (eax_pos >= 0 && (unsigned int)eax_pos < _mod.mmf_module_size)
		_mod.mmf_module_posn = eax_pos;
}

HWAVEOUT * __stdcall uFMOD_PlaySong(void *lpXM, void *param, int fdwSong)
{
	uFMOD_FreeSong();
	_mod.hInstance = (HINSTANCE)param;

	if (!lpXM)
		return NULL;

	if (fdwSong & XM_MEMORY)
	{
		_mod.uFMOD_fopen = mem_open;
		_mod.uFMOD_fread = mem_read;
		_mod.uFMOD_fclose = mem_close;
	}
	else if (fdwSong & XM_FILE)
	{
		_mod.uFMOD_fopen = file_open;
		_mod.uFMOD_fread = file_read;
		_mod.uFMOD_fclose = file_close;
	}
	else
	{
		_mod.uFMOD_fopen = res_open;
		_mod.uFMOD_fread = res_read;
		_mod.uFMOD_fclose = res_close;
	}

	if (fdwSong & XM_NOLOOP)
		_mod.ufmod_noloop = 1;

	if (fdwSong & XM_SUSPENDED)
		_mod.ufmod_pause = XM_SUSPENDED;

	if (_mod.ufmod_vol == 0)
		_mod.ufmod_vol = 32768;

	waveOutOpen(&_mod.hWaveOut, WAVE_MAPPER, (WAVEFORMATEX *)&pcm, 0, 0, 0);
	_mod.hHeap = HeapCreate(0, 0, 0);

	_mod.mmf_module_address = lpXM;

	if (LoadXM(lpXM))
	{
		uFMOD_FreeSong();
		return 0;
	}

	_mod.uFMOD_fclose();

	if (fdwSong & XM_INTEGRATION_TEST)
	{
		_mod.SW_Exit = NULL;
		_mod.ufmod_noloop = 1;
		for (;;)
		{
			uFMOD_SW_Fill();
			integration(_mod.databuf + (_mod.uFMOD_FillBlk << (FSOUND_Block+1)), FSOUND_BlockSize*2);
			if (_mod.SW_Exit)
				break;
		}
		return 0;
	}
	
	uF_WMMBlock *wavehdr = &_mod.MixBlock;
	wavehdr->dwBufferLength = FSOUND_BufferSize * sizeof(int);
	wavehdr->lpData = (LPSTR)_mod.databuf;
	wavehdr->dwFlags = 0;
	waveOutPrepareHeader(_mod.hWaveOut, wavehdr, sizeof * wavehdr);
	wavehdr->dwFlags |= WHDR_BEGINLOOP | WHDR_ENDLOOP;
	wavehdr->dwLoops = 0xffffffff;

	// ***PREFILL THE MIXER BUFFER
//loop_3:
	do
	{
		uFMOD_SW_Fill();
	} while (_mod.uFMOD_FillBlk != 0);

	_mod.SW_Exit = NULL;

	// START THE OUTPUT
	HRESULT err;
	if ((err = waveOutWrite(_mod.hWaveOut, wavehdr, sizeof * wavehdr)) != MMSYSERR_NOERROR)
	{
		uFMOD_FreeSong();
		return 0;
	}

	//ufMOD_Thread(NULL);
	//if ((_mod.hThread = (HANDLE)_beginthreadex(NULL, 0, ufMOD_Thread, NULL, 0, NULL)) == 0)
	if ((_mod.hThread = CreateThread(NULL, 0, ufMOD_Thread, 0, 0, NULL)) == NULL)
	{
		uFMOD_FreeSong();
		return 0;
	}
	SetThreadPriority(_mod.hThread, THREAD_PRIORITY_TIME_CRITICAL);

	return &_mod.hWaveOut;
}

static void thread_finished(void);
void uFMOD_FreeSong(void)
{
	if (_mod.hThread)
	{
		_mod.SW_Exit = (HANDLE)1;
		WaitForSingleObject(_mod.hThread, INFINITE);
		CloseHandle(_mod.hThread);
	}
	thread_finished();
}

static void thread_finished(void)
{
	_mod.hThread = 0;
	_mod.uFMOD_FillBlk = 0;

	if (_mod.hWaveOut)
	{
		waveOutReset(_mod.hWaveOut);
		waveOutUnprepareHeader(_mod.hWaveOut, &_mod.MixBlock, sizeof(WAVEHDR));
		waveOutClose(_mod.hWaveOut);
		_mod.hWaveOut = NULL;
	}

	if (_mod.hHeap)
	{
#ifdef USE_MEMCHECK
		checkmem();
		for (int i = 0; i < _mod.module.numpatternsmem; i++)
			free(_mod.module.pattern[i].data);

		checkmem(); 
		for (int i = 0; i < _mod.module.numinsts; i++)
			for (uF_SAMP **s = _mod.module.instrument[i].sample; *s != NULL; s++)
				free(*s);

		free(_mod.module.Channels);
		checkmem();
#endif
		/*
		for (int i = 0; i < _mod.module.numpatternsmem; i++)
			HeapFree(_mod.hHeap, 0, _mod.module.pattern[i].data);
		for (int i = 0; i < _mod.module.numinsts; i++)
			for (uF_SAMP **s = _mod.module.instrument[i].sample; *s != NULL; s++)
				HeapFree(_mod.hHeap, 0, *s);
		HeapFree(_mod.hHeap, 0, _mod.module.Channels);
		*/
		HeapDestroy(_mod.hHeap);
		ufmemset(_mod.RealBlock, 0, 4);
		_mod.time_ms = 0;
		ufmemset(_mod.szTtl, 0, 24);
		ufmemset(_mod.tInfo, 0, uF_STATS_size * totalblocks);
		_mod.hHeap = NULL;
	}
	_mod.SW_Exit = (HANDLE)1;
}

static unsigned int __stdcall ufMOD_Thread(void *p)
{
	unsigned int c;
	MMTIME mmt = { TIME_BYTES };
	do
	{
//thread_loop_1:
		MMRESULT err = waveOutGetPosition(_mod.hWaveOut, &mmt, sizeof mmt);
		assert(err == MMSYSERR_NOERROR);
		assert(mmt.wType == TIME_BYTES);

		c = mmt.u.cb;
		c >>= (FSOUND_Block + 2);
		c &= fragmentsmask;

thread_loop_2:
		// *** TAKE A LITTLE NAP : -)
		Sleep(5);
		
		// *** CHECK FOR A REQUEST TO QUIT
		if (_mod.SW_Exit)
			return 0;

//thread_loop_2_continue:;
	// *** DO WE NEED TO FETCH ANY MORE DATA INTO SOUND BUFFERS ?
	} while (_mod.uFMOD_FillBlk == c);

	if (_mod.ufmod_pause == 0)
	{
		_mod.RealBlock[0]++;
		if (_mod.RealBlock[0] < totalblocks)
			_mod.RealBlock[0] = 0;
	}
	checkmem();
//thread_readblock_ok:
	uFMOD_SW_Fill();
	checkmem();
	//todo ugly
	goto thread_loop_2;

	return 0;
}

static void uFMOD_SW_Fill(void)
{
	uF_MOD *module = &_mod.module;
	unsigned int blocksizetomix;

	// MIXBUFFER CLEAR
	ufmemset(_mod.MixBuf, 0, FSOUND_BlockSize * 2 * sizeof(int));

	if (_mod.ufmod_pause != 0)
		goto do_swfill;

	unsigned int samplesleft = module->mixer_samplesleft;
	// UPDATE MUSIC
	unsigned int *mixbuff = _mod.MixBuf;

	blocksizetomix = FSOUND_BlockSize;
	do
	{
		checkmem();

//fill_loop_1:
		if (samplesleft == 0)
		{
			int row;
			int order;
			uF_PAT *patternbase;
			uF_PAT *pattern;
			unsigned int speed;

			// UPDATE XM EFFECTS
			patternbase = module->pattern;
			if (module->tick == 0)
			{
				order = module->nextorder;
				row = module->nextrow;
				module->nextorder = -1;
				module->nextrow = -1;

				if (order >= 0)
					module->order = order;
//fill_nextrow:
				if (row >= 0)
					module->row = row;
//update_note:
				checkmem();
				pattern = DoNote(module, patternbase);
				checkmem();

				if (module->nextrow != -1)
					goto inc_tick;

				row = module->row;
				row++;

				// if end of pattern
				// "if(mod->nextrow >= mod->pattern[mod->orderlist[mod->order]].rows)"
				if (row >= pattern->rows)
				{
					int order = module->order;
					int numorders = module->numorders;
					order++;
					row = 0;
					if (order >= numorders)
					{
						if (_mod.ufmod_noloop != 0)
						{
							//thread_finished();
							uFMOD_FreeSong();
							return;
						}
//set_restart:
						order = module->restart;
						if (order >= numorders)
							order = 0;
					}
//set_nextorder:
					module->nextorder = order;
				}

//set_nextrow:
				module->nextrow = row;
			}
			else
			{
				DoEffs(module);
				checkmem();
			}
inc_tick:
			speed = module->speed;
			samplesleft = module->mixer_samplespertick;
			module->tick++;
			speed += module->patterndelay;
			if (module->tick >= speed)
			{
				module->patterndelay = 0;
				module->tick = 0;
			}
		}
//mixedleft_nz:;
		unsigned int samplestomix;
		samplestomix = blocksizetomix;
		if (samplesleft < samplestomix)
			samplestomix = samplesleft;
//fill_ramp:;
		
		unsigned int *mixbuffend;
		blocksizetomix -= samplestomix;
		mixbuffend = mixbuff + samplestomix * 2;

		Ramp(module, samplestomix, mixbuff, mixbuffend);
		mixbuff = mixbuffend;

		{
		// time_ms += SamplesToMix * FSOUND_OOMixRate * 1000
			int64_t tmp;
			tmp = (int64_t)samplestomix * 20;
			_mod.time_ms += (int)(tmp / (FSOUND_MixRate / 50));;
		}

		samplesleft -= samplestomix;

	} while (blocksizetomix != 0);

	{
		uF_STATS *stats;

		module->mixer_samplesleft = samplesleft;

		stats = &_mod.tInfo[_mod.uFMOD_FillBlk];
		stats->s_row = module->row;
		stats->s_order = module->order;
	}

do_swfill:
	{
		unsigned int *mixbuff = _mod.MixBuf;
		// ebx: L channel RMS volume
		// ebp: R channel RMS volume
		unsigned int Lvol = 0;
		unsigned int samplehi = 0;
		unsigned int blk = _mod.uFMOD_FillBlk;
		blk <<= FSOUND_Block + 2;
		unsigned short *databuff = (short *)((char *)_mod.databuf + blk);
		unsigned int ecx = FSOUND_BlockSize * 2;
		int edi;
		int esi;
		unsigned int Rvol = 0;
		int64_t tmp;
		unsigned int samplelo;

//fill_loop_2:
		do
		{
			samplelo = *mixbuff++;
			samplehi = cdq(samplelo);
			edi = samplelo;
			samplelo ^= samplehi;
			esi = (255 * volumerampsteps) / 2;
			samplelo -= samplehi;
			tmp = samplelo;
			samplelo = (int)(tmp / esi);
			samplehi = tmp % esi;
			int C = samplehi < (255 * volumerampsteps / 4);
			samplelo -= - 1 + C;
			C = samplelo < 0x8000;
			samplehi -= samplehi + C;
			samplehi = ~samplehi;
			samplelo |= samplehi;
			edi >>= 31;
			samplelo &= 0x7fff;
			tmp = (int64_t)samplelo * _mod.ufmod_vol;
			samplelo = (unsigned int)tmp;
			samplehi = tmp >> 32;
			samplelo >>= 15;

			// sum.the L and R channel RMS volume
			//ecx = rotate_right32(ecx, 1);
			samplehi -= samplehi + (ecx&1);
			samplehi &= samplelo;
			Rvol += samplehi; // += |vol|
			//ecx = rotate_left32(ecx, 1);
			samplehi -= samplehi + (ecx&1);
			samplehi = ~samplehi;
			samplehi &= samplelo;
			Lvol += samplehi; // += |vol|
			samplelo ^= edi;
			samplelo -= edi;
			edi = esi;

			*databuff++ = samplelo;

			ecx--;
		} while (ecx != 0);

//SW_Fill_Ret:

		blk = _mod.uFMOD_FillBlk;
		blk++;
		if (blk >= totalblocks)
			blk = 0;

//SW_Fill_R:
		_mod.uFMOD_FillBlk = blk;
		_mod.tInfo[blk].L_vol = Lvol >> FSOUND_Block;
		_mod.tInfo[blk].R_vol = Rvol >> FSOUND_Block;
	}
#ifdef USE_DUMP
	diagnostics(&_mod);
#endif
}

static const unsigned char sin127[] =
{
	0x00, 0x0C, 0x19, 0x25, 0x31, 0x3C, 0x47, 0x51, 0x5A, 0x62, 0x6A, 0x70, 0x75, 0x7A, 0x7D, 0x7E,
	0x7F, 0x7E, 0x7D, 0x7A, 0x75, 0x70, 0x6A, 0x62, 0x5A, 0x51, 0x47, 0x3C, 0x31, 0x25, 0x19, 0x0C
};

static const unsigned char sin64[] =
{
	0x00, 0x02, 0x03, 0x05, 0x06, 0x08, 0x09, 0x0B, 0x0C, 0x0E, 0x10, 0x11, 0x13, 0x14, 0x16, 0x17,
	0x18, 0x1A, 0x1B, 0x1D, 0x1E, 0x20, 0x21, 0x22, 0x24, 0x25, 0x26, 0x27, 0x29, 0x2A, 0x2B, 0x2C,
	0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x38, 0x39, 0x3A, 0x3B,
	0x3B, 0x3C, 0x3C, 0x3D, 0x3D, 0x3E, 0x3E, 0x3E, 0x3F, 0x3F, 0x3F, 0x40, 0x40, 0x40, 0x40, 0x40,
	0xAB /* sometimes this is read off the end of the array in asm version */
};

static const float f0_0833 = 8.3333336e-2f;
static const float f13_375 = 1.3375e1f;
static const float f0_0013 = 1.302083375e-3f;
static const float f8363_0 = 8.3630004275e3f;

static int Ramp_calculate_loop_count(uF_SAMP *sample, FSOUND_CHANNEL *channel, unsigned int *length);
static int Ramp_mix(uF_SAMP *sample, FSOUND_CHANNEL *channel, unsigned int *length, unsigned int **mixbuffer, unsigned int *endofmixbuffer);
static int Ramp_update_loops(uF_SAMP *sample, FSOUND_CHANNEL *channel);

static void Ramp(uF_MOD *module, int lengthX, unsigned int *mixbufferX, unsigned int *mixbuffend)
{
	FSOUND_CHANNEL *channel;
	uF_SAMP *sample;
	
	channel = module->Channels;
	do
	{
//loop_ch:
		sample = channel->fsptr;
		if (sample == NULL)
			goto MixExit;

//CalculateLoopCount:
		unsigned int *mixbuffer = mixbufferX;
		int length = lengthX;
		do
		{
			for (;;)
			{
				int c = Ramp_calculate_loop_count(sample, channel, &length);
				if (c == -1)
					goto MixExit;
				if (c == 1)
					break;//jmp DoOutputbuffEnd

				if (Ramp_mix(sample, channel, &length, &mixbuffer, mixbuffend))
					break;//jmp DoOutputbuffEnd
			}

//DoOutputbuffEnd:
			if (_mod.mix_endflag[0] != 0)
				goto MixExit;

			if (Ramp_update_loops(sample, channel))
				break; //jmp MixExit

		} while (length != 0);

MixExit:

		channel++;
	} while (channel < (FSOUND_CHANNEL *)module->uFMOD_Ch);//works only because they were allocated with the same alloc

	checkmem();
}

static int Ramp_calculate_loop_count(uF_SAMP *sample, FSOUND_CHANNEL *channel, unsigned int *length)
{
	unsigned int mixposlo;
	unsigned int speedlo;
	int mixposhi;
	int speedhi;
	int edx;
	int64_t tmp;

	// Set up a mix counter.See what will happen first, will the output buffer
	// end be reached first ? or will the end of the sample be reached first ? whatever
	// is smallest will be the mixcount.

	mixposhi = channel->mixpos;
	edx = sample->loopstart;
	mixposlo = channel->mixposlo;
	if (channel->speeddir == 0)
	{
		// work out how many samples left from mixpos to loop end
		edx += sample->looplen;
		edx -= mixposhi;
		if (edx <= 0)
		{
			edx = sample->_length;
			edx -= mixposhi;
		}
//submixpos:
		// edx: samples left(loopstart + looplen - mixpos)
		edx = -edx;
		mixposlo = (~mixposlo)+1;
		edx += mixposhi + (mixposlo != 0);
	}

//samplesleftbackwards:
	// work out how many samples left from mixpos to loop start

	edx = -edx;
	edx += mixposhi;
	if (edx < 0)
		return -1;//js MixExit

	// edx:eax now contains number of samples left to mix
	speedlo = channel->speedlo;
	mixposlo = shrd(mixposlo, edx, 5);
	speedhi = channel->speedhi;
	edx >>= 5;
	speedlo = shrd(speedlo, speedhi, 5);
	if (speedlo == 0)
	{
		channel->speedlo = FREQ_40HZ_f;
		speedlo = FREQ_40HZ_p;
	}
//speedok:
	tmp = ((int64_t)edx << 32)+mixposlo;
	mixposlo = (int)(tmp / speedlo);
	edx = tmp % speedlo;
	speedlo = 0;
	mixposlo += speedlo+(edx!=0);
	if (mixposlo == 0)
		return 1; //jz DoOutputbuffEnd

	// set a flag to say mix will end when end of output buffer is reached
	_mod.mix_endflag[0] = mixposlo > *length;

	if (mixposlo < *length)
		*length = mixposlo;

//staywithoutputbuffend:
	{
		unsigned int rampcount;
		//unsigned int mixposhi;
		int leftvol;
		int rightvol;

		rampcount = channel->ramp_count;
		// VOLUME RAMP SETUP
		// Reasons to ramp
		// 1 volume change
		// 2 sample starts(just treat as volume change - 0 to volume)
		// 3 sample ends(ramp last n number of samples from volume to 0)
		// now if the volume has changed, make end condition equal a volume ramp
		leftvol = channel->leftvolume;
		rightvol = channel->rightvolume;
		rightvol <<= 16;
		rightvol |= leftvol;
		_mod.mmf_mixcount = *length; //remember mix count before modifying it //todo check

		if (rampcount == 0)
			goto volumerampstart;

		if (rightvol == channel->ramp_LR_target)
			goto volumerampclamp;

volumerampstart:
		// SETUP NEW RAMP
		channel->ramp_LR_target = rightvol;
		
		leftvol <<= volumeramps_pow;
		leftvol -= channel->ramp_leftvolume;
		leftvol >>= volumeramps_pow;
		channel->ramp_leftspeed = leftvol;
		if (leftvol != 0)
			rampcount = volumerampsteps;

//novolumerampL:
		rightvol &= 0xffff0000;
		rightvol >>= (16 - volumeramps_pow);
		rightvol -= channel->ramp_rightvolume;
		rightvol >>= volumeramps_pow;
		channel->ramp_rightspeed = rightvol;
		if (rightvol != 0)
			rampcount = volumerampsteps;

//novolumerampR:
		channel->ramp_count = rampcount;
		if (rampcount != 0)
		{
volumerampclamp:
			if (*length <= rampcount)
				goto volumerampend;

			*length = rampcount;
		}
volumerampend:

		_mod.ramp_leftspeed = channel->ramp_leftspeed;
		_mod.ramp_rightspeed = channel->ramp_rightspeed;
		_mod.mmf_length = *length;//todo
	}
	return 0;
}
	
static int Ramp_mix(uF_SAMP *sample, FSOUND_CHANNEL *channel, unsigned int *length, unsigned int **mixbufferptr, unsigned int *endofmixbuffer)
{
	// SET UP ALL OF THE REGISTERS HERE FOR THE INNER LOOP
	// edx: speed
	// ebx: mixpos
	// ebp: speed low
	// esi: destination pointer
	// edi: counter

	unsigned int *mixbuffer = *mixbufferptr;
	short *mixpos;
	mixpos = (short *)((char *)sample + channel->mixpos * 2 + offsetof(uF_SAMP, buff));

	//todo, refactor into 64bit math
	int speedhi = channel->speedhi;
	unsigned int speedlo = channel->speedlo;

	if (channel->speeddir == 0)
		goto MixLoop16;

	speedlo = (~speedlo) + 1;
	speedhi = -speedhi-(speedlo!=0);

MixLoop16:
	while ((*length)--)
	{
		int s0, s1;
		unsigned int mixposlo;
		int64_t tmp;
		int mix;
		unsigned int s1lo;

		s0 = *mixpos;
		s1 = *(mixpos + 1);

		// interpolate between s0 and s1 by mixposlo
		mixposlo = channel->mixposlo;
		s1 -= s0;
		mixposlo >>= 1;//31 bit unsigned

		tmp = s1 * (int64_t)mixposlo;//edx:eax = 31+16 bits signed
		s1 = (tmp << volumeramps_pow) >> 32;//31+16+7-32 = 16+6 bits signed
		s0 <<= volumeramps_pow - 1;//16+6 bits signed 
		mix = s0 + s1;

		tmp = mix * (int64_t)channel->ramp_rightvolume;//edx:sx = 31+16 bits signed
		s1 = (int)(tmp >> (volumeramps_pow - 1));//31+16-6-32 = 31+10-32 = 9 bits signed
		s1lo = rotate_left32((tmp>>32), 1);
		s1lo &= 1;//sign bit of s1
		s1 += s1lo;
		s1 >>= 1;
		*mixbuffer++ += s1;

		tmp = mix * (int64_t)channel->ramp_leftvolume;
		s1 = (int)(tmp >> (volumeramps_pow - 1));
		s1lo = rotate_left32((tmp>>32), 1);
		s1lo &= 1;
		s1 += s1lo;
		s1 >>= 1;
		*mixbuffer++ += s1;

		channel->ramp_rightvolume += _mod.ramp_rightspeed;
		channel->ramp_leftvolume += _mod.ramp_leftspeed;

		{
			uint64_t tmp = (uint64_t)channel->mixposlo + speedlo;
			channel->mixposlo = (unsigned int)tmp;
			mixpos += speedhi + (int)(tmp >> 32);
		}
	}

	// find out how many OUTPUT samples left to mix
	*length = (unsigned int)(ptrdiff_t)((char *)endofmixbuffer - (char *)mixbuffer) / 8;

	//turn mixpos pointer back into a sample number (when it's signed, top bit must be cleared by shift)
	channel->mixpos = (unsigned int)((char *)mixpos - (char *)&sample->buff) >> 1;

	*mixbufferptr = mixbuffer;

	{
		// DID A VOLUME RAMP JUST HAPPEN ?
		int ramp;
		int rampcount;

		rampcount = channel->ramp_count;
		if (rampcount == 0)
			return 1;

		ramp = _mod.mmf_length;// length
		channel->ramp_count -= ramp;
		if (channel->ramp_count != 0)
			return 1;

		ramp -= _mod.mmf_mixcount;// mixcount
		channel->ramp_leftspeed = 0;
		channel->ramp_rightspeed = 0;
		rampcount -= rampcount+(ramp!=0);
		if ((rampcount & *length) == 0)
			return 1;
	}

	return 0;
}

static int Ramp_update_loops(uF_SAMP *sample, FSOUND_CHANNEL *channel)
{
	int loopmode;
	unsigned int speedlo;
	unsigned int mixpos;
//DoOutputbuffEnd:

	mixpos = channel->mixpos;
	speedlo = channel->speedlo;

	//00 - no loop
	//01 - loop forward
	//10 - ping-pong (bi-directional)
	//11 - reserved

	loopmode = sample->loopmode;

	// SWITCH ON LOOP MODE TYPE
	loopmode--;
	if (loopmode == 0)// check for normal loop(FSOUND_LOOP_NORMAL = 1)
	{
		int rampcount = 0;
		unsigned int loopstart = sample->loopstart;
		unsigned int looplen = sample->looplen;
		loopstart += looplen;
		if (mixpos > loopstart)
		{
			unsigned int tmp_3;
			mixpos -= loopstart;
			tmp_3 = loopstart;
			loopstart = mixpos;
			mixpos = tmp_3;
			rampcount = loopstart % looplen;
		}
//rewind_ok:
		looplen -= rampcount;
		mixpos -= looplen;
		if (mixpos <= 0)
			mixpos = 0;
		goto ChkLoop_OK;
	}

//CheckBidiLoop:
	loopmode--; // FSOUND_LOOP_BIDI = 2

	if (loopmode != 0)
	{
		channel->mixposlo = 0;
		channel->mixpos = 0;
		channel->fsptr = 0;
		return 1;//jz MixExit
	}

	if (channel->speeddir == loopmode) // FSOUND_MIXDIR_FORWARDS
		goto BidiForward;

//BidiBackwards:
	speedlo = (~speedlo) + 1;
	for (;;)
	{
		unsigned int loopstart;
		unsigned int loopend_m1;
		uint64_t tmp;
		int overflow;

		loopstart = sample->loopstart;
		speedlo = (~speedlo) + 1;
		loopstart--;
		speedlo--;
		channel->speeddir--; // set FSOUND_MIXDIR_FORWARDS
		loopstart -= mixpos+(speedlo==0xffffffff);
		mixpos = sample->loopstart;
		mixpos += loopstart;
		if ((int)loopstart < (int)sample->looplen)
			break;

BidiForward:
		tmp = (uint64_t)sample->loopstart;
		tmp += sample->looplen;
		loopstart = (unsigned int)tmp;
		overflow = tmp >> 32;
		loopend_m1 = loopstart - 1;
		loopstart -= mixpos+overflow;
		speedlo = (~speedlo) + 1;
		mixpos = loopstart;
		speedlo--;
		overflow = speedlo == 0xffffffff;
		mixpos += loopend_m1 + overflow;
		channel->speeddir++;// go backwards
		if (mixpos >= sample->loopstart)
			break;
	}
//BidiFinish:

	channel->mixposlo = speedlo;

ChkLoop_OK:
	channel->mixpos = mixpos;

	return 0;
}

//work out difference between two pitches, and lerp with the finetune value
static int AmigaPeriod(uF_SAMP *sample, unsigned int note)
{
	int eax;
	int edx, ecx;
	unsigned int edi, esi;
	int tmp;

	esi = 1;
	edx = 132 - note;
	tmp = edx;

	eax = sample->finetune;
	edi = edx;
	
	if (note != 0)
		edx = cdq(eax)<<1;

//_do_inc:
	edx++;

//exp2:
	do
	{
		
		/*
		__asm ("fild dword [tmp]");
		__asm ("fmul dword [f0_0833]");
		__asm ("fld st(0)");
		__asm ("frndint");
		__asm ("fsub st(1), st(0)");
		__asm ("fxch st(1)");
		__asm ("f2xm1");
		__asm ("fld1");
		__asm ("faddp st(1)");
		__asm ("fscale");
		__asm ("fstp st(1)");
		__asm ("fmul dword [f13_375]");
		__asm ("fistp dword [tmp]");
		*/

		/*
		multiply by 0.0833
		separate fraction and int parts of number
		add the powers of 2 of each part
		multiply by 13.375

		return (int)(f13_375 * powf(2.0f, (float)tmp * f0_0833));
		*/

#ifdef INLINE_X86

#ifdef USE_TABLE_HZ
		ecx = pow2_table_AmigaPeriod(tmp);
#endif

		__asm {
			fild [tmp]
			fmul [f0_0833]
			fld st(0)
			frndint
			fsub st(1), st(0)
			fxch st(1)
			f2xm1
			fld1
			faddp st(1),st(0)
			fscale
			fstp st(1)
			fmul [f13_375]
			fistp [tmp]
		};

#ifdef USE_TABLE_HZ
		assert(tmp == ecx);
#endif

#else

#ifdef USE_TABLE_HZ
		tmp = pow2_table_AmigaPeriod(tmp);
#else
		tmp = (int)(0.5f + f13_375 * pow2f((float)tmp * f0_0833));
#endif

#endif

		ecx = tmp;

		if (esi == 0)
			break;

		esi = 0;
		edi -= edx;

		tmp = edi;

		edi = ecx;
	} while (1);

//exp2_end:
	{
		int offset;
		offset = ecx - edi;
		if (edx < 0)
			offset = -offset;

//_do_imul:
		int64_t tmp = (int64_t)eax * offset;
		eax = (unsigned int)tmp;
		edx = (tmp >> 32) & 0x7f;
		eax += edx;
		eax >>= 7;
		eax += edi;
	}
	return eax;
}

static int VibratoOrTremolo(uF_CHAN *ufchan)
{
	unsigned int C2;
	unsigned int vibpos;
	int newvibpos;

	vibpos = ufchan->vibpos;
	C2 = ufchan->wavecontrol;
	newvibpos = vibpos;
	newvibpos += ufchan->vibspeed;
	newvibpos &= 0x3f;

	C2 &= 3;
	ufchan->vibpos = newvibpos;
	if (C2 != 0)
	{
		//C2: square
		vibpos = rotate_left32(vibpos, 0x1b);
		newvibpos -= newvibpos;
		newvibpos ^= 0x7f;
		newvibpos |= 1;
		C2--;
		if (C2 != 0)
			goto vibrato_default;

		//C1 : triangle
		newvibpos >>= 0x18;
		vibpos = rotate_right32(vibpos, 0x17);
		newvibpos += vibpos;
		assert(0);
		newvibpos = ~newvibpos;
	}
	else
	{
		//C0: sine wave
		newvibpos = sin127[vibpos&0x1f];
		if (vibpos & 0x20)
			newvibpos = -newvibpos;
	}

vibrato_default:
	return (newvibpos * ufchan->vibdepth) >> 5;
}

static void Vibrato(uF_CHAN *ufchan)
{
	int eax = VibratoOrTremolo(ufchan);
	eax >>= 1;
	ufchan->freqdelta = eax;
	ufchan->notectrl |= FMUSIC_FREQ;
}

static void Tremolo(uF_CHAN *ufchan)
{
	//hacky!
	uF_CHAN *hackyufchan = (uF_CHAN *)((char *)ufchan + offsetof(uF_CHAN, tremolopos) - offsetof(uF_CHAN, vibpos));
	int eax = VibratoOrTremolo(hackyufchan);
	ufchan->voldelta = eax;
	ufchan->notectrl |= FMUSIC_VOLUME;
}

static void Portamento(uF_CHAN *ufchan)
{
	int freq, portatarget, portaspeed;

	ufchan->notectrl |= FMUSIC_FREQ;
	
	freq = ufchan->freq;
	portatarget = ufchan->portatarget;
	portaspeed = ufchan->portaspeed;
	portaspeed <<= 2;
	
	freq -= portatarget;
	
	if (freq > 0)
		goto porta_do_sub;
	freq += portaspeed;
	
	if (freq > 0)
		goto _do_trim;
	goto _no_trim;

porta_do_sub:
	freq -= portaspeed;
	if (freq < 0)
		goto _do_trim;

_no_trim:
	portatarget += freq;

_do_trim:
	ufchan->freq = portatarget;
}

static void Envelope(uF_CHAN *ufchan, uF_INST *instrument, unsigned int eax)
{
	unsigned int ebx, panvol_numpoints, panvol_type;
	unsigned int tmp_0, edi, ecx;
	unsigned int env_pos;
	int env_next;
	
	signed char sustain_l1;
	signed char sustain_l2;
	signed char sustain_loop;

	uF_VOLPAN *vp;
	uF_VOLPANFRAC *vpf;
	unsigned char *vps;
	unsigned short *pts;

	ebx = 0;

	ufchan->notectrl |= eax;

	sustain_loop = instrument->PANsustain;
	sustain_l1 = instrument->PANLoopStart;
	sustain_l2 = instrument->PANLoopEnd;
	panvol_type = instrument->PANtype;
	panvol_numpoints = instrument->PANnumpoints;

	vp = &ufchan->envpan;
	vpf = &ufchan->envpanfrac;
	vps = &ufchan->envpanstopped;
	pts = instrument->PANPoints;

	if (eax == FMUSIC_VOLUME)
	{
		sustain_loop = instrument->VOLsustain;
		sustain_l1 = instrument->VOLLoopStart;
		sustain_l2 = instrument->VOLLoopEnd;
		panvol_type = instrument->VOLtype;
		panvol_numpoints = instrument->VOLnumpoints;

		vp = &ufchan->envvol;
		vpf = &ufchan->envvolfrac;
		vps = &ufchan->envvolstopped;
		pts = instrument->VOLPoints;
	}

//pan_or_vol_ok:
	if (*vps == 0)
	{
		// if (*pos >= numpoints) envelop out of bound
		unsigned int pos = vp->pos;
		unsigned short tick;
		if (pos >= panvol_numpoints)
			goto envelope_done;

		//if (*tick == points[(*pos) << 1]) we are at the correct tick for the position
		tick = pts[pos * 2];
		if (vp->tick != tick)
			goto add_envdelta;

		if ((panvol_type & FMUSIC_ENVELOPE_LOOP) != 0)
		{
			// if((type&FMUSIC_ENVELOPE_LOOP) && *pos == loopend) handle loop
			if (vp->pos != sustain_l2)
				goto loop_ok;

			vp->pos = sustain_l1;
			vp->tick = pts[vp->pos * 2];
		}
loop_ok:
		{
			unsigned int pos = vp->pos;
			env_pos = pos;
			unsigned short *points = &pts[pos * 2];
			unsigned int ebx;

			panvol_numpoints--;//edx
			ebx = *points;// get tick at this point
			
			edi = *(unsigned int *)(points + 2);
			eax = *(points + 1);

			env_next = edi;// get tick at next point

			vpf->val = eax;

			if (vp->pos != panvol_numpoints)
				goto env_continue;

			(*vps)++;
		}
	}
//goto_envelope_ret:
	goto Envelope_Ret;

env_continue:
	eax <<= 16;
	edi -= eax;
	tmp_0 = eax;
	eax = edi;
	edi = tmp_0;
	eax &= 0xffff0000;
	if ((panvol_type & FMUSIC_ENVELOPE_SUSTAIN) != 0)
	{
		panvol_numpoints = sustain_loop;
		if (env_pos != panvol_numpoints)
			goto not_sustain;
		
		if (ufchan->keyoff == 0)
			goto Envelope_Ret;
	}
not_sustain:
	// interpolate 2 points to find delta step

	vp->pos++;
	vpf->frac = edi;
	ecx = (int)env_next&0x0000ffff;
	vp->delta = 0;
	ecx -= vp->tick;
	if (ecx != 0)
	{
		vp->delta = (int)eax / (int)ecx;
		goto envelope_done;
	}

add_envdelta:

	// interpolate
	vpf->frac += vp->delta;
	
envelope_done:
	
	vp->tick++;
	vpf->val = vpf->frac>>16;// *value = *valfrac >> 16
Envelope_Ret:;
}

static void VolByte(uF_CHAN *ufchan, int volparam)
{
	unsigned int tmp_0;
	
	unsigned int volcommand;

	volparam -= 0x10;
	if (volparam >= 0 && volparam <= 0x40)
		ufchan->volume = volparam;

	int parm = volparam;
	//todo refactor into switch
//switch_volume:
	volcommand = volparam;
	volparam &= 0xf;
	volcommand >>= 4;
	volcommand -= 5;
	if (volcommand != 0)
	{
		volcommand--;
		if (volcommand == 0)
		{
			goto case_7;
		}
		volcommand--;
		if (volcommand == 0)
		{
			goto case_6;
		}
		volcommand--;
		if (volcommand == 0)
		{
			goto case_7;
		}
		volcommand -= 2;
		if (volcommand == 0 || volcommand == 0xffffffff)
		{
			goto case_AB;
		}
		volcommand--;
		if (volcommand == 0)
		{
			goto case_C;
		}
		volcommand--;
		if (volcommand == 0)
		{
			goto case_D;
		}
		volcommand--;
		if (volcommand == 0)
		{
			goto case_E;
		}
		volcommand--;
		if (volcommand == 0)
		{
			volparam <<= 4;
			if (volparam != 0)
			{
				ufchan->portaspeed = volparam;
			}
//vol_z:
			ufchan->notectrl &= NOT_FMUSIC_TRIGGER;
			volcommand = ufchan->period;
			ufchan->portatarget = volcommand;
		}
//vol_default:
		return;
	}
case_6:
	volparam = -volparam;
case_7:
	ufchan->volume += volparam;
	return;

case_AB:
	//command is 0 or -1
	if (volcommand == 0xffffffff)
		ufchan->vibpos = volparam;
	else
		ufchan->vibspeed = volparam;
	return;

case_C:
	volparam <<= 4;
	ufchan->pan = volparam;
	tmp_0 = volcommand;
	volcommand = volparam;
	volparam = tmp_0;
case_D:
	volparam = -volparam;
case_E:
	ufchan->pan += volparam;
	ufchan->notectrl |= FMUSIC_PAN;
}

static void Tremor(uF_CHAN *ufchan)
{
	//int ecx;
	unsigned char dl, dh;
	ufchan->notectrl |= FMUSIC_VOLUME;
	dl = ufchan->tremorpos;
	dh = ufchan->tremoron;
	if (dl <= dh)
		goto inc_pos;

	ufchan->voldelta = -ufchan->volume;

inc_pos:
	dh += ufchan->tremoroff;
	if (dl <= dh)
		goto Tremor_Ret;
	dl = -1;
Tremor_Ret:
	dl++;
	ufchan->tremorpos = dl;
}

static void SetBPM(int bpm)
{
	unsigned int eax = (FSOUND_MixRate * 5) / 2;
	if (bpm != 0)
		eax /= bpm;
	_mod.module.mixer_samplespertick = eax;
}

static void loadxm_load_pats(uF_MOD *module, unsigned int loadxm_numpat, unsigned int loadxm_fnumpat);
static void loadxm_for_instrs(uF_MOD *module);
static void loadxm_for_samp(uF_INST *instrument, int samplecount, int sampleheadersize);
static void loadxm_for_loadsamp(uF_INST *instrument, int samplecount);

// definitive XM format, thanks to the guys at MilkyTracker!
// https://github.com/milkytracker/MilkyTracker/blob/master/resources/reference/xm-form.txt
// https://www.fileformat.info/format/xm/corion.htm
static int LoadXM(void *lpXM)
{
	int channelcount;
	int loadxm_fnumpat;
	unsigned int loadxm_numpat;

	uF_MOD *module;
	uF_XM xm;

	unsigned int numpat = 0;

	_mod.uFMOD_fopen(lpXM);
	if (!_mod.SW_Exit)
		return 1;

	module = &_mod.module;

	//read the XM and module headers
	_mod.uFMOD_fread(&xm, uF_XM_size);
	swabufxm(&xm);

	_mod.uFMOD_fread(&module->header_size, uF_MOD_size-offsetof(uF_MOD, header_size));
	swabufmod(&module);

	// GOTO PATTERN DATA
	uFMOD_lseek(module->header_size + uF_XM_size, 0, UFMOD_LSEEK_SET);

	{
		char *src = xm.modulename;
		char *dst = _mod.szTtl;
		unsigned int len = 20;
//loadxm_ttl:
		while (len--)
		{
			char c = *src++;
			if (c >= 0x20) // copy only printable chars
				*dst++ = c;
		}
		*dst++ = '\0';
		*dst++ = '\0';
		*dst++ = '\0';
		*dst++ = '\0';
	}

	{
		// COUNT NUM.OF PATTERNS
		int numorders = module->numorders;
		unsigned int fnumpat = module->numpatternsmem;

		//ebx = -ebx;//sets carry flag if ebx!=0
		//edx -= edx;//0 if ebx is 0, else -1 (0xffffffff)
		//ecx &= edx;//0 if ebx (=> edx) is 0, else ecx
		//ebx = -ebx;//put ebx back
		//same as
		if (fnumpat == 0)
			numorders = 0;

		numorders--;
		loadxm_fnumpat = fnumpat;

		if (numorders < 0 || numorders > 255)
			return 1;

		unsigned char *orders = module->orderlist;
//loadxm_for_pats:
		do
		{
			unsigned char order = *orders++;
			if (order > numpat)
				numpat = order;
			numorders--;
		} while (numorders >= 0);
		loadxm_numpat = numpat;
		numpat++;

		// ALLOCATE THE PATTERN ARRAY (whatever is bigger : fnumpat or numpat) & CHANNEL POOL
		if (fnumpat > numpat)
			numpat = fnumpat;
	}

	unsigned int patternsize;
	module->numpatternsmem = numpat;
	patternsize = numpat * uF_PAT_size;

//loadxm_pats_ok2:
	{
		unsigned int instsize = module->numinsts * uF_INST_size;
		unsigned int buffsize = instsize + numpat * uF_PAT_size;
		unsigned char *buff;
		unsigned int channelsize;
		FSOUND_CHANNEL *channel;
		uF_CHAN *ufchan;

		unsigned int numchannels = module->numchannels_xm;

		if (numchannels > 64)
			numchannels = 0;

//loadxm_numchan_ok:
		module->numchannels = numchannels;
		if (numchannels == 0)
			return 1;

		channelsize = numchannels * FSOUND_CHANNEL_size*2;

		buffsize += channelsize + numchannels * uF_CHAN_size;

		buff = alloc(buffsize);

		module->Channels = channel = (FSOUND_CHANNEL *)buff;
		module->uFMOD_Ch = ufchan = (uF_CHAN *)(buff + channelsize);

//loop_2:
		channelcount = numchannels;
		while (channelcount--)
		{
			channel->speedhi = 1;
			ufchan->cptr = channel;

			channel += 2;
			ufchan++;
		}

		//patterns follow channels
		module->pattern = (uF_PAT *)ufchan;
		//instruments follow patterns
		module->instrument = (uF_INST *)((char *)ufchan + patternsize);
	}

	SetBPM(module->defaultbpm);

	module->globalvolume = 64;
	module->speed = module->defaultspeed;

	loadxm_load_pats(module, loadxm_numpat, loadxm_fnumpat);
	loadxm_for_instrs(module);

	return 0;
}

static void loadxm_load_pats(uF_MOD *module, unsigned int loadxm_numpat, unsigned int loadxm_fnumpat)
{
	unsigned int loadxm_count1=0;
	uF_PAT *pattern = module->pattern;
	do
	{
		uF_NOTE *nptr;
		uF_PATTERN_HEADER pat_hdr;

		_mod.uFMOD_fread(&pat_hdr, uF_PATTERN_HEADER_size);
		swabufpathdr(&pat_hdr);

		// ALLOCATE PATTERN BUFFER
		pattern->patternsize = pat_hdr.loadxm_pat_size;
		pattern->rows = pat_hdr.rowcount;

		if (pat_hdr.loadxm_pat_size == 0) // skip an empty pattern
			goto loadxm_ldpats_continue;

		int loadxm_count2 = module->numchannels * pat_hdr.rowcount;

		pattern->data = alloc(loadxm_count2 * uF_NOTE_size);

		nptr = pattern->data;
//loadxm_for_rowsxchan:
		do
		{
			signed char note;
			_mod.uFMOD_fread(&note, 1);
			if (note & 0x80)
			{
				if (note & 1)
					_mod.uFMOD_fread(&nptr->unote, 1);
				if (note & 2)
					_mod.uFMOD_fread(&nptr->number, 1);
				if (note & 4)
					_mod.uFMOD_fread(&nptr->uvolume, 1);
				if (note & 8)
					_mod.uFMOD_fread(&nptr->effect, 1);
				if (note & 16)
					_mod.uFMOD_fread(&nptr->eparam, 1);
			}
			else
			{
				if (note != 0)
					nptr->unote = note;
				_mod.uFMOD_fread(&nptr->number, 4);//read remaining unpacked note data
			}

//loadxm_isnote97:
			if (nptr->number > module->numinsts)
				nptr->number = 0;
			nptr++;
			loadxm_count2--;
		} while (loadxm_count2 != 0);

loadxm_ldpats_continue:
		loadxm_count1++;
		pattern++;
	} while (loadxm_count1 < loadxm_fnumpat);

	// allocate and clean out any extra patterns
	if (loadxm_numpat >= loadxm_fnumpat)
	{
		uF_PAT *extrapatterns;
		unsigned int emptypatternsize;

		extrapatterns = module->pattern + loadxm_numpat;
		emptypatternsize = module->numchannels * 64 * uF_NOTE_size;

//loadxm_for_extrapats:
		do
		{
			extrapatterns->rows = 64;
			extrapatterns->data = alloc(emptypatternsize);
			extrapatterns--;
			loadxm_numpat--;
		} while (loadxm_numpat >= loadxm_fnumpat);
	}

//loadxm_extrapats_ok:
	module->mixer_samplesleft = 0;
	module->tick = 0;
	module->patterndelay = 0;
	module->nextorder = 0;
	module->nextrow = 0;

	if (module->numinsts == 0)
		return;
}

static void loadxm_for_instrs(uF_MOD *module)
{
	int loadxm_count1;
	uF_INST *instrument;

	for (loadxm_count1 = 0, instrument = module->instrument; loadxm_count1 < module->numinsts; loadxm_count1++, instrument++)
	{
		uF_INSTRUMENT_HEADER inst_hdr;
		unsigned int esi;
		unsigned int dl;

		_mod.uFMOD_fread(&inst_hdr, uF_INSTRUMENT_HEADER_size);
		swabufinsthdr(&inst_hdr);

		esi = inst_hdr.headersize;
		dl = inst_hdr.samplecount;
		esi -= uF_INSTRUMENT_HEADER_size;

		if (dl != 0)
		{
			if (dl > 0x10)
				return;//todo

			if (inst_hdr.sampleheadersize > 40)
				return;//todo

			esi -= 208;
			_mod.uFMOD_fread(&instrument->keymap, 208);//this is the remaining instrument header (241-33)
			swabufinst(instrument);
		}

//loadxm_inst_ok:
		uFMOD_lseek(esi, 0, UFMOD_LSEEK_CUR);

		instrument->VOLfade = instrument->VOLfade << 1;

		if (instrument->VOLnumpoints < 2)
			instrument->VOLtype = 0;

		if (instrument->PANnumpoints < 2)
			instrument->PANtype = 0;

//loadxm_PANtype_ok:
		if (inst_hdr.samplecount != 0)
		{
			loadxm_for_samp(instrument, inst_hdr.samplecount, inst_hdr.sampleheadersize);
			loadxm_for_loadsamp(instrument, inst_hdr.samplecount);
		}
//loadx_for_loadsamp_end:
	}
}

static void loadxm_for_samp(uF_INST *instrument, int samplecount, int sampleheadersize)
{
	int samplenumber;
	for (samplenumber = 0; samplenumber < samplecount; samplenumber++)
	{
		uF_SAMPLE_HEADER sample_hdr;
		unsigned char is16;
		uF_SAMP *sampledataptr;

		_mod.uFMOD_fread(&sample_hdr, sampleheadersize);//sizeof uF_SAMPLE_HEADER or less
		swabufsamphdr(&sample_hdr);

		sample_hdr.loadxm_s0loopmode = sample_hdr.loadxm_s0bytes[0];
		is16 = (sample_hdr.loadxm_s0loopmode >> 4) & 1;
		sample_hdr.loadxm_s0bytes[0] = is16;//todo: messy
		if (is16 != 0)
		{
			sample_hdr.loadxm_sample_2 >>= 1;
			sample_hdr.loadxm_s0loopstart >>= 1;
			sample_hdr.loadxm_s0looplen >>= 1;
		}

//loadxm_s0bytes_ok:
		if (sample_hdr.loadxm_s0loopstart > sample_hdr.loadxm_sample_2)
			sample_hdr.loadxm_s0loopstart = sample_hdr.loadxm_sample_2;
//loadxm_loopstart_ok:
		if (sample_hdr.loadxm_s0loopstart + sample_hdr.loadxm_s0looplen > sample_hdr.loadxm_sample_2)
			sample_hdr.loadxm_s0looplen = sample_hdr.loadxm_sample_2 - sample_hdr.loadxm_s0loopstart;

//loadxm_looplen_ok:
		sample_hdr.loadxm_s0loopmode &= SAMPLE_LOOP_MASK;
		if (sample_hdr.loadxm_s0loopmode == SAMPLE_LOOP_TYPE_NONE || sample_hdr.loadxm_s0looplen == 0)
		{
			//there's a loop type, but the length of it is 0, or there's no loop
			sample_hdr.loadxm_s0loopstart = 0;
			sample_hdr.loadxm_s0looplen = sample_hdr.loadxm_sample_2;
			sample_hdr.loadxm_s0loopmode = 0;
		}

		sampledataptr = alloc(sample_hdr.loadxm_sample_2 * 2 + uF_SAMP_size + 4);
		instrument->sample[samplenumber] = sampledataptr;

		//copy over the first 20 bytes of the header
		//ufmemcpy(eax, &sample_hdr, 20);
		sampledataptr->_length = sample_hdr.loadxm_sample_2;//4
		sampledataptr->loopstart = sample_hdr.loadxm_s0loopstart;//8
		sampledataptr->looplen = sample_hdr.loadxm_s0looplen;//12
		sampledataptr->defvol = sample_hdr.volume;//13
		sampledataptr->finetune = sample_hdr.finetune;//14
		sampledataptr->bytes = sample_hdr.loadxm_s0bytes[0];//15  NB. this is modified from sample_hdr
		sampledataptr->defpan = sample_hdr.loadxm_s0bytes[1];//16
		sampledataptr->relative = sample_hdr.loadxm_s0bytes[2];//17
		sampledataptr->Resved = sample_hdr.loadxm_s0bytes[3];//18
		sampledataptr->loopmode = sample_hdr.loadxm_s0loopmode;//19
		sampledataptr->_align = 0;//20
	}
}

static void loadxm_for_loadsamp(uF_INST *instrument, int samplecount)
{
	int samplenumber = 0;
	for (samplenumber = 0; samplenumber < samplecount; samplenumber++)
	{
		uF_SAMP *sample;
		unsigned char ch;
		unsigned char cl;
		unsigned int eax;
		int length;
		unsigned char *sampledata;
		unsigned char cmp[16];

		sample = instrument->sample[samplenumber];
		eax = 0;
		length = sample->_length;
		ch = sample->Resved;
		cl = sample->bytes;
		if ((length & 0xffc00000) != 0)//4,194,303 max sample size
			return;//todo

		sampledata = (unsigned char*)&sample->buff;

		if (ch != MODPLUG_4BIT_ADPCM) // ModPlug 4-bit ADPCM
			goto loadxm_regular_samp;

		// unpacked length
		length = ((length + 1) >> 1) * 3;
		sampledata += length;

		// Read in the compression table
		_mod.uFMOD_fread(cmp, 16);

		// Read the sample data 
		_mod.uFMOD_fread(sampledata, (sample->_length+1)>>1);

//loadxm_unpack_loop:
		{
			// Decompress sample data
			unsigned int *dst = (unsigned int *)&sample->buff;
			int ecx = 0;

			do
			{
				unsigned char al, ah;

				if ((unsigned char *)dst >= sampledata)
					break;

				al = *sampledata++;
				ah = al;
				al &= 0xf;
				al = cmp[al];
				ah >>= 4;
				ch += al;//todo ch is part of ecx
				al = ah;
				al = cmp[al];
				al += ch;
				eax <<= 24;
				ecx |= eax;
				*dst++ = ecx;
				ecx >>= 16;
			} while (1);
		}
//loadxm_unpack_ok:

		goto loadxm_chk_loop_bidi;

loadxm_regular_samp:
		_mod.uFMOD_fread(&sample->buff, sample->_length << sample->bytes);//sample->bytes is 0 or 1

		if (sample->bytes != 0)
		{
			swabsample(&sample->buff, sample->_length);
			goto loadxm_16bit_ok;
		}

		// Promote to 16 bits
//loadxm_to16bits:
		{
			unsigned char *src = sampledata + sample->_length;
			unsigned short *dst = (unsigned short *)(src + sample->_length);

			do
			{
				*--dst = (unsigned short)*--src << 8;
			} while ((unsigned char *)dst > src);
		}

loadxm_16bit_ok:

//loadxm_do_delta_conv:
		{
			unsigned short dx = 0;
			unsigned short *src = (unsigned short *)sampledata;
			int len = sample->_length;

			// Do delta conversion
			do
			{
				dx += *src;
				*src++ = dx;
				len--;
			} while (len > 0);
		}
		//goto loadxm_loops_ok; // branch never taken in asm

loadxm_chk_loop_bidi:
		{
			unsigned short cx;
			unsigned short *src;

			src = (unsigned short *)sampledata + sample->loopstart + sample->looplen;
			if (sample->loopmode == 2) // LOOP_BIDI
			{
				cx = src[-1];
			}
			else
			{
				if (sample->loopmode != 1) // LOOP_NORMAL
				{
					goto loadxm_loops_ok;
				}
				cx = *((unsigned short *)sampledata + sample->loopstart);
			}
//loadxm_fix_loop:
			*src = cx;
		}

loadxm_loops_ok:;
	}
}

const void (*S1_TBL[])(uF_CHAN *, int);

static uF_PAT *DoNote(uF_MOD *module, uF_PAT *pattern)
{
	unsigned int numchannels;
	uF_NOTE *noteptr;

	uF_INST *donote_iptr;
	int donote_oldfreq;
	int donote_currtick;
	int donote_oldpan;
	unsigned int donote_porta;
	int donote_jumpflag;
	uF_SAMP *donote_sptr;

	uF_PAT *patternptr;
	uF_CHAN *ufchan;

	patternptr = &pattern[module->orderlist[module->order]];

	numchannels = module->numchannels;
	donote_jumpflag = 0;

	if (patternptr->data == 0)
		return patternptr;

	noteptr = patternptr->data + module->row * module->numchannels;

	if (numchannels == 0)
		return patternptr;

	ufchan = module->uFMOD_Ch;// +numchannels;

//donote_for_channels:
	do
	{
		int effect, eparam, noteno, instno;

		eparam = noteptr->eparam;
		effect = noteptr->effect;
		eparam &= 0xf;
		instno = noteptr->number;

		donote_porta = 0;
		if (effect == FMUSIC_XM_PORTATO || effect == FMUSIC_XM_PORTATOVOLSLIDE)
		{
			donote_porta = 1;
			goto donote_rem_note;
		}

		instno--;
		if (instno >= 0)
			ufchan->inst = instno;

		noteno = noteptr->unote;
		noteno--;
		if (noteno >=0 && noteno < 96)
			ufchan->note = noteno;

donote_rem_note:;
		unsigned int key;
		unsigned int fx;

		donote_iptr = &module->instrument[ufchan->inst];

		key = donote_iptr->keymap[ufchan->note];

		if (key < 16 && donote_iptr->sample[key])
			donote_sptr = donote_iptr->sample[key];
		else
			donote_sptr = &_mod.DummySamp;

//donote_valid_sptr:

		donote_oldfreq = ufchan->freq;
		donote_currtick = ufchan->volume;
		donote_oldpan = ufchan->pan;

		// if there is no more tremolo, set volume to volume + last tremolo delta
		fx = noteptr->effect;
		if (fx != FMUSIC_XM_TREMOLO)
		{
			if (ufchan->recenteffect != FMUSIC_XM_TREMOLO)
			{
				goto donote_tremolo_vol;
			}
			ufchan->volume += ufchan->voldelta;
		}

donote_tremolo_vol:
		ufchan->recenteffect = fx;
		ufchan->voldelta = 0;
		ufchan->freqdelta = 0;
		ufchan->notectrl = FMUSIC_VOLUME_OR_FREQ;
		// PROCESS NOTE
		checkmem();
		{
			int note;
			note = noteptr->unote;
			note--;
			if (note >= 0 && note < 96)
			{
				int edx, eax;
				note += donote_sptr->relative;
				ufchan->realnote = note;
				if ((_mod.module.flags & 1) != 0)
				{
					eax = donote_sptr->finetune;
					edx = cdq(eax);
					note <<= 6;
					eax -= edx;
					eax >>= 1;
					eax = note + eax - 0x1e00;
					eax = -eax;
				}
				else
				{
					eax = AmigaPeriod(donote_sptr,note);
				}

				ufchan->period = eax;

				if (donote_porta == 0)
					ufchan->freq = eax;
				
				ufchan->notectrl |= FMUSIC_TRIGGER;
			}
		}
		{
			// PROCESS INSTRUMENT NUMBER
			checkmem();
			if (noteptr->number != 0)
			{
				uF_SAMP *sample;
				int wavecontrol;
				sample = donote_sptr;

				ufchan->volume = sample->defvol;
				ufchan->pan = sample->defpan;
				ufchan->envvolfrac.val = 64;
				ufchan->envpanfrac.val = 32;
				ufchan->fadeoutvol = 0x10000;

				//9 dwords from envvoltick
				//ufmemset(edi, eax, ecx);
				ufchan->envpan.tick = 0;
				ufchan->envpan.pos = 0;
				ufchan->envpan.delta = 0;
				ufchan->envvol.tick = 0;
				ufchan->envvol.pos = 0;
				ufchan->envvol.delta = 0;
				ufchan->ivibsweeppos = 0;
				ufchan->ivibpos = 0;
				ufchan->keyoff = 0;
				ufchan->envvolstopped = 0;
				ufchan->envpanstopped = 0;

				wavecontrol = ufchan->wavecontrol;

				if (wavecontrol < 0x4f)
					ufchan->tremolopos = 0;

				if ((wavecontrol & 0x0C) == 0)
					ufchan->vibpos = 0;

				ufchan->notectrl |= FMUSIC_VOLUME_OR_PAN;
			}
		}
//donote_zcptr_ok:

		// PROCESS VOLUME BYTE
		checkmem();
		VolByte(ufchan, noteptr->uvolume);
		// PROCESS KEY OFF
		if (noteptr->unote <= 96)
		{
			if (noteptr->effect != FMUSIC_XM_KEYOFF)
				goto donote_keyoff_ok;
		}
		ufchan->keyoff++;

donote_keyoff_ok:
		{
			// PROCESS ENVELOPES
			checkmem();
			if ((donote_iptr->VOLtype & 1) != 0)
			{
				Envelope(ufchan, donote_iptr, FMUSIC_VOLUME);
			}
			else
			{
				if (ufchan->keyoff == 0)
					goto donote_volenv_ok;

				ufchan->envvolfrac.val = 0;
			}
donote_volenv_ok:

			if ((donote_iptr->PANtype & 1) != 0)
			{
				Envelope(ufchan,donote_iptr, FMUSIC_PAN);
			}
		}

//donote_no_pantype:
		{
			// PROCESS VOLUME FADEOUT
			checkmem();
			if (ufchan->keyoff != 0)
			{
				ufchan->fadeoutvol -= donote_iptr->VOLfade;
				if (ufchan->fadeoutvol >= 0)
				{
					goto donote_fadevol_ok;
				}
				ufchan->fadeoutvol = 0;
			}
		}

donote_fadevol_ok:
		{
		// PROCESS TICK 0 EFFECTS
			checkmem();
			unsigned int effect = noteptr->effect;
			effect--; // skip FMUSIC_XM_ARPEGGIO

			//hacks for 9,11,13,21,25
			_mod.S1.donote_jumpflag = donote_jumpflag;
			_mod.S1.donote_sptr = donote_sptr;
			_mod.S1.donote_iptr = donote_iptr;

			//hacks for 14.13
			_mod.S1.S2_C13.donote_oldfreq = donote_oldfreq;
			_mod.S1.S2_C13.donote_oldpan = donote_oldpan;
			_mod.S1.S2_C13.donote_currtick = donote_currtick;

			if (effect <= 0x20)
				S1_TBL[effect](ufchan, noteptr->eparam);
			checkmem();
			DoFlags(ufchan, donote_iptr, donote_sptr);
			checkmem();
		}

		noteptr++;
		ufchan++;
	} while (--numchannels);

	checkmem();
	return patternptr;
}


static void S1_r(uF_CHAN *ufchan, int dl){}

static void S1_C1(uF_CHAN *, int);
static void S1_C3(uF_CHAN *, int);
static void S1_C4(uF_CHAN *, int);
static void S1_C5(uF_CHAN *, int);
static void S1_C6(uF_CHAN *, int);
static void S1_C7(uF_CHAN *, int);
static void S1_C8(uF_CHAN *, int);
static void S1_C9(uF_CHAN *, int);
static void S1_C10(uF_CHAN *, int);
static void S1_C11(uF_CHAN *, int);
static void S1_C12(uF_CHAN *, int);
static void S1_C13(uF_CHAN *, int);
static void S1_C14(uF_CHAN *, int);
static void S1_C15(uF_CHAN *, int);
static void S1_C16(uF_CHAN *, int);
static void S1_C17(uF_CHAN *, int);
static void S1_C21(uF_CHAN *, int);
static void S1_C25(uF_CHAN *, int);
static void S1_C27(uF_CHAN *, int);
static void S1_C29(uF_CHAN *, int);
static void S1_C33(uF_CHAN *, int);

static const void(*S1_TBL[])(uF_CHAN *, int) =
{
	S1_C1,
	S1_C1,
	S1_C3,
	S1_C4,
	S1_C5,
	S1_C6,
	S1_C7,
	S1_C8,
	S1_C9,
	S1_C10,
	S1_C11,
	S1_C12,
	S1_C13,
	S1_C14,
	S1_C15,
	S1_C16,
	S1_C17,
	S1_r, // unassigned effect ordinal[18]
	S1_r, // unassigned effect ordinal[19]
	S1_r, // skip FMUSIC_XM_KEYOFF
	S1_C21,
	S1_r, // unassigned effect ordinal[22]
	S1_r, // unassigned effect ordinal[23]
	S1_r, // unassigned effect ordinal[24]
	S1_C25,
	S1_r, // unassigned effect ordinal[26]
	S1_C27,
	S1_r, // unassigned effect ordinal[28]
	S1_C29,
	S1_r, // unassigned effect ordinal[30]
	S1_r, // unassigned effect ordinal[31]
	S1_r, // unassigned effect ordinal[32]
	S1_C33
};

static void S1_C1(uF_CHAN *channel, int param)
{
	if (param)
		channel->portaupdown = param;
}

static void S1_C3(uF_CHAN *channel, int param)
{
	if (param)
		channel->portaspeed = param;

	channel->notectrl &= NOT_FMUSIC_TRIGGER_OR_FRQ;
	channel->portatarget = channel->period;
}

static void S1_C5(uF_CHAN *channel, int param)
{
	if (param)
		channel->volslide = param;

	channel->notectrl &= NOT_FMUSIC_TRIGGER_OR_FRQ;
	channel->portatarget = channel->period;
}

static void S1_C4(uF_CHAN *channel, int param)
{
	int ebx = param & 0xf;
	param >>= 4;
	if (param == 0)
		goto donote_vib_x_ok;

	channel->vibspeed = param;

donote_vib_x_ok:
	if (ebx == 0)
		goto donote_vib_y_ok;

	channel->vibdepth = ebx;

donote_vib_y_ok:
	
	Vibrato(channel);
}

static void S1_C6(uF_CHAN *channel, int param)
{
	if (param)
		channel->volslide = param;
	Vibrato(channel);
}

static void S1_C7(uF_CHAN *channel, int param)
{
	int ebx = param & 0xf;
	param >>= 4;
	if (param == 0)
		goto donote_trem_x_ok;
	
	channel->tremolospeed = param;

donote_trem_x_ok:
	if (ebx != 0)
		channel->tremolodepth = ebx;

//donote_trem_y_ok:
}

static void S1_C9(uF_CHAN *ufchan, int param)
{
	int eax;
	uF_SAMP *sample;

	param <<= 8;
	if (param == 0)
		goto donote_soffset_ok;

	ufchan->sampleoffset = param;

donote_soffset_ok:

	sample = _mod.S1.donote_sptr;
	param = sample->loopstart;
	param += sample->looplen;
	eax = ufchan->sampleoffset;

	FSOUND_CHANNEL *channel = ufchan->cptr;
	if (eax < param)
		goto donote_set_offset;

	eax = 0;
	ufchan->notectrl &= NOT_FMUSIC_TRIGGER;
	channel->mixpos = 0;
	channel->mixposlo = 0;

donote_set_offset:
	channel->fsampleoffset = eax;
}

static void S1_C10(uF_CHAN *channel, int param)
{
	if (param)
		channel->volslide = param;
}

static void S1_C11(uF_CHAN *channel, int param)
{
	uF_MOD *module = &_mod.module;

	module->nextrow = 0;
	module->nextorder = param;

	_mod.S1.donote_jumpflag++;

//donote_set_nextord:
	if (module->nextorder >= module->numorders)
		module->nextorder = 0;

//donote_nextorder_ok:
}

static void S1_C13(uF_CHAN *channel, int param)
{
	int ecx;
	int ebx = param & 0xf;

	param >>= 4;
	param = param * 5;
	ecx = ebx + param * 2;// paramx * 10 + paramy (it's BCD)

	uF_MOD *module = &_mod.module;
	module->nextrow = ecx;

	if (_mod.S1.donote_jumpflag != 0)
		return;
	
	module->nextorder = module->order+1;

//donote_set_nextord:
	if (module->nextorder >= module->numorders)
		module->nextorder = 0;

//donote_nextorder_ok:;
}

static void S1_C15(uF_CHAN *channel, int param)
{
	if (param >= 0x20)
		goto donote_setbpm;

	_mod.module.speed = param;
	return;

donote_setbpm:;
	SetBPM(param);
}

static void S1_C17(uF_CHAN *channel, int param)
{
	if (param)
		_mod.module.globalvsl = param;
}

static void S1_C21(uF_CHAN *channel, int param)
{
	uF_INST *instrument;
	int ecx;
	int eax;
	int edx = param;
	int donote_currtick;
	int numpoints;
	short *volpts;

	instrument = _mod.S1.donote_iptr;
	volpts = instrument->VOLPoints + 2;

	if (!(instrument->VOLtype & 1))
		return;

	// Search and reinterpolate new envelope position
	numpoints = instrument->VOLnumpoints;
	eax = 0;
	if (param <= *volpts)
		goto donote_env_endwhile;
donote_envwhile:
	if (eax == numpoints) //if (currpos == iptr->VOLnumpoints) break;
		goto donote_env_endwhile;
	eax++;
	if (param > volpts[eax * 2]) //if (current->eparam > iptr->VOLPoints[(currpos + 1) << 1]) break;
		goto donote_envwhile;
donote_env_endwhile:

	channel->envvol.pos = eax;
	// if it is at the last position, abort the envelope and continue last volume
	numpoints--; 
	channel->envvolstopped = (eax >= numpoints);
	if (eax < numpoints)
		goto donote_env_continue;
	eax = volpts[numpoints * 2-1];
	channel->envvolfrac.val = eax;// cptr->envvol = iptr->VOLPoints[((iptr->VOLnumpoints - 1) << 1) + 1];
//donote_envelope_r:
	return;

donote_env_continue:
	channel->envvol.tick = param;
	ecx = volpts[eax * 2 - 2]; //get tick at this point + VOL at this point;
	edx = ecx;
	ecx &= 0x0000ffff;
	donote_currtick = ecx;
	ecx = volpts[eax * 2]; //get tick at next point + VOL at next point
	eax = ecx;
	ecx &= 0x0000ffff;
	edx = 0;
	// interpolate 2 points to find delta step;
	ecx -= donote_currtick;
	int efx = edx;
	if (ecx == 0)
		goto donote_no_tickdiff;
	eax = 0;
	eax -= edx;
	int64_t tmp = eax;
	tmp /= ecx;
	ecx = eax;

donote_no_tickdiff:
	channel->envvol.delta = ecx;
	eax = channel->envvol.tick;
	eax -= donote_currtick;
	eax *= ecx;
	edx = efx;
	eax += edx;
	channel->envvolfrac.frac = eax;
	eax >>= 16;
	channel->envvolfrac.val = eax&0xffff;
	channel->envvol.pos++;
}

static void S1_C25(uF_CHAN *channel, int param)
{
	if (param)
	{
		channel->panslide = param;
		channel->notectrl |= FMUSIC_PAN;
	}
}

static void S1_C27(uF_CHAN *channel, int param)
{
	if (param)
		channel->retrigx = ((param>>4)&0xf)|((param&0xf)<<4);
}

static void S1_C29(uF_CHAN *channel, int param)
{
	if(param)
		channel->tremoron = ((param >> 4) & 0xf) | ((param & 0xf) << 4);

	Tremor(channel);
}

static void S1_C33(uF_CHAN *channel, int param)
{
	int eax;
	int bl = param & 0xf;
	param >>= 4;
	param--;
	if (param != 0)
		goto donote_paramx_n1;

	if (bl == 0)
		goto donote_paramy_z1;
	
	channel->xtraportaup = bl;

donote_paramy_z1:
	eax = channel->xtraportaup;
	channel->freq -= eax;

donote_paramx_n1:
	param--;
	if (param != 0)
		goto donote_paramx_n2;

	if (bl == 0)
		goto donote_paramy_z2;

	channel->xtraportadown = bl;

donote_paramy_z2:
	eax = channel->xtraportadown;
	channel->freq += eax;

donote_paramx_n2:;
}

static void S2_r(uF_CHAN *esi, int edx) {};

static void S2_C1(uF_CHAN *, int);
static void S2_C2(uF_CHAN *, int);
static void S2_C4(uF_CHAN *, int);
static void S2_C5(uF_CHAN *, int);
static void S2_C6(uF_CHAN *, int);
static void S2_C7(uF_CHAN *, int);
static void S2_C8(uF_CHAN *, int);
static void S2_C10(uF_CHAN *, int);
static void S2_C11(uF_CHAN *, int);
static void S2_C13(uF_CHAN *, int);
static void S2_C14(uF_CHAN *, int);

const void (*S2_TBL[])(uF_CHAN *, int);

void S1_C14(uF_CHAN *channel, int p)
{
	unsigned int param = p;
	param >>= 4;
	param--; // skip FMUSIC_XM_SETFILTER
	if (param > 13)
		return;

//donote_do_special:
	S2_TBL[param](channel, p&0xf);
}

static const void (*S2_TBL[])(uF_CHAN *, int) =
{
	S2_C1,
	S2_C2,
	S2_r,// skip FMUSIC_XM_SETGLISSANDO,
	S2_C4,
	S2_C5,
	S2_C6,
	S2_C7,
	S2_C8,
	S2_r,// skip FMUSIC_XM_RETRIG,
	S2_C10,
	S2_C11,
	S2_r,// skip FMUSIC_XM_NOTECUT,
	S2_C13,
	S2_C14,
};

static void S2_C1(uF_CHAN *channel, int param)
{
	int fineportaup;
	if (param)
		channel->fineportaup = param;

	fineportaup = channel->fineportaup;
	fineportaup <<= 2;
	channel->freq -= fineportaup;
}

static void S2_C2(uF_CHAN *channel, int param)
{
	int fineportadown;
	if(param)
		channel->fineportadown = param;

	fineportadown = channel->fineportadown;
	fineportadown <<= 2;
	channel->freq += fineportadown;
}

static void S2_C4(uF_CHAN *channel, int param)
{
	channel->wavecontrol &= 0xF0;
	channel->wavecontrol |= param;
}

static void S2_C5(uF_CHAN *channel, int param)
{
	_mod.S1.donote_sptr->finetune = param;
}

static void S2_C6(uF_CHAN *channel, int param)
{
	int patloopno;
	if (param == 0)
	{
		channel->patlooprow = _mod.module.row;
		return;
	}

	patloopno = channel->patloopno;
	patloopno--;
	if (patloopno >= 0)
		goto donote_patloopno_ok;

	patloopno = param;

donote_patloopno_ok:
	channel->patloopno = patloopno;
	if (patloopno == 0)
		goto donote_patloopno_end;

	_mod.module.nextrow = channel->patlooprow;

donote_patloopno_end:;
}

static void S2_C7(uF_CHAN *channel, int param)
{
	channel->wavecontrol &= 0x0F;
	param <<= 4;
	channel->wavecontrol = param;
}

static void S2_C8(uF_CHAN *channel, int param)
{
	param <<= 4;
	channel->pan = param;
	channel->notectrl |= FMUSIC_PAN;
}

static void S2_C10(uF_CHAN *channel, int param)
{
	S2_C11(channel, -param);
}

static void S2_C11(uF_CHAN *channel, int param)
{
	if (param)
		channel->finevslup = param;

	channel->volume -= channel->finevslup;
}

static void S2_C13(uF_CHAN *channel, int param)
{
	channel->freq = _mod.S1.S2_C13.donote_oldfreq;
	channel->pan = _mod.S1.S2_C13.donote_oldpan;
	channel->volume = _mod.S1.S2_C13.donote_currtick;
	channel->notectrl = 0;
}

static void S1_C12(uF_CHAN *channel, int param)
{
	channel->volume = param;
}

static void S2_C14(uF_CHAN *esi, int param)
{
	uF_MOD *module = &_mod.module;
	module->patterndelay = param * module->speed;
}

const void (*S3_TBL[])(uF_CHAN *, unsigned int x, unsigned int y);

static void DoEffs(uF_MOD *module)
{
	unsigned int eax;
	uF_CHAN *channel;
	uF_NOTE *doeff_current;
	uF_SAMP *sample = NULL;
	// Point our note pointer to the correct pattern buffer, and to the
	// correct offset in this buffer indicated by row andnumber of channels

	unsigned int pattern = module->orderlist[module->order];
	doeff_current = module->pattern[pattern].data;
	if (doeff_current == 0)
		return;

	doeff_current += module->row * module->numchannels;

	channel = module->uFMOD_Ch;
	int numchannels = module->numchannels;
//doeff_for_channels:
	while (numchannels--)
	{
		unsigned int key;

		uF_INST *instrument = &_mod.module.instrument[channel->inst];

		key = instrument->keymap[channel->note];
		
		if (key < 0x10)
			sample = instrument->sample[key];

		if (sample == NULL)
			sample = &_mod.DummySamp;

//doeff_valid_sptr:
		channel->voldelta = 0;
		channel->freqdelta = 0;
		channel->notectrl = 0;

		if ((instrument->VOLtype & 1) != 0)
			Envelope(channel, instrument, FMUSIC_VOLUME);

		if ((instrument->PANtype & 1) != 0)
			Envelope(channel, instrument, FMUSIC_PAN);

		// PROCESS VOLUME FADEOUT
		if (channel->keyoff != 0)
		{
			eax = instrument->VOLfade;
			channel->fadeoutvol -= eax;
			if (channel->fadeoutvol < 0)
				channel->fadeoutvol = 0;
			channel->notectrl |= FMUSIC_VOLUME;
		}

//doeff_fadevol_ok:
		{
			unsigned int volbyte = doeff_current->uvolume;
			unsigned int volctl;
			int volume;
			volctl = volbyte >> 4;
			volume = volbyte & 0xf;
			volctl -= 6;
			//todo refactor into switch
			if (volctl != 0)
			{
				volctl--;
				if (volctl != 0)
				{
					volctl -= 4;
					if (volctl != 0)
					{
						volctl--;
						volctl--;
						if (volctl != 0)
						{
							volctl--;
							if (volctl != 0)
							{
								volctl--;
								if (volctl != 0)
								{
									goto doeff_volbyte_end;
								}
								Portamento(channel);
								goto doeff_volbyte_end;
							}
							volume = -volume;
						}
						channel->pan -= volume;
						channel->notectrl |= FMUSIC_PAN;
						goto doeff_volbyte_end;
					}
					channel->vibdepth = volume;
					Vibrato(channel);
				}
			}
			else
			{
				volume = -volume;
			}
			channel->volume += volume;
			channel->notectrl |= FMUSIC_VOLUME;
		}
doeff_volbyte_end:
		{
			unsigned int eparam = doeff_current->eparam;
			unsigned int xparam, yparam;
			unsigned int effect = doeff_current->effect;
			yparam = eparam & 0xf;
			xparam = eparam >>= 4;
			//hack for S3_C0, S3_C14
			_mod.S3.doeff_current = doeff_current;
			_mod.S3.sample = sample;
			if (effect <= 0x1d)
				S3_TBL[effect](channel, xparam, yparam);
		}
//doeff_s3_brk:
		DoFlags(channel, instrument, sample);

		channel++;
		doeff_current++;
	}
//doeff_R:;
}

static void S3_r(uF_CHAN *esi, unsigned int ecx, unsigned int ebx) { }
static void S3_C0(uF_CHAN *, unsigned int, unsigned int);
static void S3_C1(uF_CHAN *, unsigned int, unsigned int);
static void S3_C2(uF_CHAN *, unsigned int, unsigned int);
static void S3_Portamento(uF_CHAN *channel, unsigned int x, unsigned int y) { Portamento(channel); }
static void S3_Vibrato(uF_CHAN *channel, unsigned int x, unsigned int y) { Vibrato(channel); }
static void S3_C5(uF_CHAN *, unsigned int, unsigned int);
static void S3_C6(uF_CHAN *, unsigned int, unsigned int);
static void S3_Tremolo(uF_CHAN *channel, unsigned int x, unsigned int y) { Tremolo(channel); }
static void S3_C10(uF_CHAN *, unsigned int, unsigned int);
static void S3_C14(uF_CHAN *, unsigned int, unsigned int);
static void S3_C17(uF_CHAN *, unsigned int, unsigned int);
static void S3_C25(uF_CHAN *, unsigned int, unsigned int);
static void S3_C27(uF_CHAN *, unsigned int, unsigned int);
static void S3_Tremor(uF_CHAN *channel, unsigned int x, unsigned int y) { Tremor(channel); }

static const void (*S3_TBL[])(uF_CHAN *, unsigned int ecx_xparam, unsigned int ebx_yparam) = {
	S3_C0,
	S3_C1,
	S3_C2,
	// cptr + 2 : ESI,
	S3_Portamento,
	// cptr + 2 : ESI,
	S3_Vibrato,
	S3_C5,
	S3_C6,
	// cptr + 2 : ESI,
	S3_Tremolo,
	S3_r, // skip FMUSIC_XM_SETPANPOSITION,
	S3_r, // skip FMUSIC_XM_SETSAMPLEOFFSET,
	S3_C10,
	S3_r, // skip FMUSIC_XM_PATTERNJUMP,
	S3_r, // skip FMUSIC_XM_SETVOLUME,
	S3_r, // skip FMUSIC_XM_PATTERNBREAK,
	S3_C14,
	S3_r, // skip FMUSIC_XM_SETSPEED,
	S3_r, // skip FMUSIC_XM_SETGLOBALVOLUME,
	S3_C17,
	S3_r, // unassigned effect ordinal[18],
	S3_r, // unassigned effect ordinal[19],
	S3_r, // skip FMUSIC_XM_KEYOFF,
	S3_r, // skip FMUSIC_XM_SETENVELOPEPOS,
	S3_r, // unassigned effect ordinal[22],
	S3_r, // unassigned effect ordinal[23],
	S3_r, // unassigned effect ordinal[24],
	S3_C25,
	S3_r, // unassigned effect ordinal[26],
	S3_C27,
	S3_r, // unassigned effect ordinal[28],
	//case FMUSIC_XM_TREMOR,
	//cptr: ESI,
	S3_Tremor,
};

static void S3_C6(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	Vibrato(channel);
	S3_C10(channel, ecx_xparam, ebx_yparam);
}

static void S3_C5(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	Portamento(channel);
	S3_C10(channel, 0, ebx_yparam);
}

static void S3_C10(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	int eax;
	int volslide = channel->volslide;
	eax = volslide;
	eax &= 0x0F;
	volslide >>= 4;
	if (volslide == 0)
		goto doeff_volslide_ok;

	// Slide up takes precedence over down
	eax = volslide;
	eax = -eax;

doeff_volslide_ok:
	channel->volume -= eax;
	channel->notectrl |= FMUSIC_VOLUME;
}

static void DoFreqSlide(uF_CHAN *channel, int portaupdown)
{
	int freq = channel->freq;
	freq += portaupdown * 4;
	if (freq < 1)
		freq = 1;

	channel->freq = freq;
	channel->freqdelta = 0;
	channel->notectrl |= FMUSIC_FREQ;
}

static void S3_C2(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	DoFreqSlide(channel, channel->portaupdown);
}

static void S3_C1(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	DoFreqSlide(channel, -channel->portaupdown);
}

static void S3_C0(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	uF_NOTE *edx=_mod.S3.doeff_current;

	if (edx->eparam <= (ecx_xparam>>8))
		goto doeff_arpeggio_ok;

	int tick = _mod.module.tick;
	int tickmod3;
	tickmod3 = tick % 3;
	tickmod3--;
	if (tickmod3 == 0)
		goto doeff_arpeg_c1;

	tickmod3--;
	if (tickmod3 != 0)
		goto doeffs_enable_freq;
	
	ecx_xparam = ebx_yparam;

doeff_arpeg_c1:
	if ((_mod.module.flags & 1) == 0)
		goto doeffs_flagsn1;

//doeffs_arpeggio_freqd:
	ecx_xparam <<= 6;
	channel->freqdelta = ecx_xparam;
	goto doeffs_enable_freq;

doeffs_flagsn1:
	{
		uF_SAMP *sptr = _mod.S3.sample;
		int v0, v1;

		v0 = AmigaPeriod(sptr, channel->realnote);
		v1 = AmigaPeriod(sptr, channel->realnote + ecx_xparam);

		channel->freqdelta = v1-v0;
	}
doeffs_enable_freq:

	channel->notectrl |= FMUSIC_FREQ;

doeff_arpeggio_ok:;
}

const int (*S4_TBL[])(int);

static void S3_C27(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	int retrigy, volume;
	
	retrigy = channel->retrigy;
	if (retrigy == 0)
		goto doeff_multiretrig_ok;

	int64_t tick = _mod.module.tick % retrigy;
	if (tick != 0)
		goto doeff_multiretrig_ok;

	retrigy= channel->retrigx;
	retrigy &= 0x0F
		;
	channel->notectrl|= FMUSIC_TRIGGER;
	
	retrigy--;
	volume = channel->volume;
	
	if (retrigy < 0)
		goto doeff_multiretrig_ok;

	channel->notectrl|= FMUSIC_VOLUME;
	channel->volume = S4_TBL[retrigy](volume);

doeff_multiretrig_ok:;
}

static int S4_C2(int vol) { return vol - 2; }
static int S4_C1(int vol) { return vol - 1; }
static int S4_C5(int vol) { return vol - 16; }
static int S4_C4(int vol) { return vol - 8; }
static int S4_C3(int vol) { return vol - 4; }
static int S4_C6(int vol) { return (vol * 2) / 3; }
static int S4_C14(int vol) { return (vol * 3) / 2; }
static int S4_C7(int vol) { return vol / 2; }
static int S4_C10(int vol) { return vol + 2; }
static int S4_C9(int vol) { return vol + 1; }
static int S4_C13(int vol) { return vol + 16; }
static int S4_C12(int vol) { return vol + 8; }
static int S4_C11(int vol) { return vol + 4; }
static int S4_C15(int vol) { return vol * 2; }
static int S4_r(int vol) { return vol; }

static const int (*S4_TBL[])(int) = {
	S4_C1,
	S4_C2,
	S4_C3,
	S4_C4,
	S4_C5,
	S4_C6,
	S4_C7,
	S4_r,
	S4_C9,
	S4_C10,
	S4_C11,
	S4_C12,
	S4_C13,
	S4_C14,
	S4_C15
};

static void S3_C25(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	unsigned int panparam,panslide;
	int pan;
	panslide = channel->panslide;
	panparam = panslide;
	panparam &= 0x0F;
	pan = channel->pan;
	panslide >>= 4;
	
	// Slide right takes precedence over left;
	if (panslide == 0)
	{
		pan -= panparam;
		if (pan >= 0)
			pan = panslide;
	}
	else
	{
//doeff_pan_slide_right:
		panslide += pan;
		pan = 255;
		if (panslide <= pan)
			pan = panslide;
	}
//doeff_panslide_ok:

	S1_C8(channel, pan);
}

static void S1_C8(uF_CHAN *channel, int edx)
{
	channel->pan = edx;
	channel->notectrl |= FMUSIC_PAN;
}

static void S3_C17(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	uF_MOD *ecx = &_mod.module;
	int volume;
	int vsl;
	volume = ecx->globalvolume;
	vsl = ecx->globalvsl;
	
	if ((vsl & 0xf0) != 0) // Slide up takes precedence over down;
		goto doeff_gvsl_slide_up;
	
	vsl &= 0x0F;
	vsl = -vsl;
	goto set_global_vol;

doeff_gvsl_slide_up:
	vsl >>= 4;

set_global_vol:
	volume += vsl;
	_mod.module.globalvolume = volume;
}

static void S1_C16(uF_CHAN *esi, int edx)
{
	_mod.module.globalvolume = edx;
}

static void S3_C14(uF_CHAN *channel, unsigned int ecx_xparam, unsigned int ebx_yparam)
{
	int eax;
	unsigned int edx;

	//todo, refactor into switch
	if (ecx_xparam == FMUSIC_XM_RETRIG)
		goto doeff_do_retrig;

	ecx_xparam -= FMUSIC_XM_NOTECUT;
	if (ecx_xparam == 0)
		goto  doeff_do_notecut;

	ecx_xparam--;
	if (ecx_xparam != 0)
		goto doeff_special_r;
	
	eax = 0;
	if (_mod.module.tick != ebx_yparam)
		goto doeff_notectrl_z;
	
	uF_SAMP *sample = _mod.S3.sample;

	channel->fadeoutvol = 65536;
	edx = sample->defvol;
	ecx_xparam = eax + 9;
	channel->volume = edx;
	edx = channel->envvolfrac.val;

	if (ecx_xparam < edx)
		edx = 64;

	channel->envvolfrac.val = edx;

	channel->envvol.tick = 0;
	channel->envvol.pos = 0;
	channel->envvol.delta = 0;
	channel->envpan.tick = 0;
	channel->envpan.pos = 0;
	channel->envpan.delta = 0;
	channel->ivibsweeppos = 0;
	channel->ivibpos = 0;
	channel->keyoff = 0;
	channel->envvolstopped = 0;
	channel->envpanstopped = 0;

	// Retrigger tremolo and vibrato waveforms;
	ecx_xparam = channel->wavecontrol;
	if (ecx_xparam >= 0x4F)
		goto z2_tremolopos_ok;
	channel->tremolopos = 0;

z2_tremolopos_ok:
	if (ecx_xparam & 0x0C)
		goto z2_vibpos_ok;
	channel->vibpos = 0;

z2_vibpos_ok:
	eax = channel->period;
	channel->notectrl |= FMUSIC_VOL_OR_FREQ_OR_TR;
	channel->freq = eax;

	{
		uF_NOTE *eax = _mod.S3.doeff_current;
		VolByte(channel, eax->uvolume);
	}

	return;

doeff_notectrl_z:
	channel->notectrl = eax;

doeff_special_r:
	return;

doeff_do_notecut:
	if (_mod.module.tick != ebx_yparam)
		goto doeff_notecut_ok;

	channel->volume = 0;
	channel->notectrl |= FMUSIC_VOLUME;

doeff_notecut_ok:
	return;

doeff_do_retrig:
	if (ebx_yparam == 0)
		goto doeff_retrig_ok;

	edx = _mod.module.tick % ebx_yparam;

	if (edx != 0)
		goto doeff_retrig_ok;
	channel->notectrl |= FMUSIC_VOL_OR_FREQ_OR_TR;

doeff_retrig_ok:;
}

static void DoFlags(uF_CHAN *ufchan, uF_INST *flags_iptr, uF_SAMP *flags_sptr)
{
	unsigned int tmp_3;
	unsigned int tmp_4;
	unsigned int tmp_5;
	unsigned int ecx;
	int edx, eax;
	uF_INST *instrument;
	unsigned int *ivibposptr;

	// THIS GETS ADDED TO PREVIOUS FREQDELTAS
	instrument = flags_iptr;
	eax = 0;
	edx = instrument->VIBtype;
	ivibposptr = &ufchan->ivibpos;

	edx--;
	if (edx < 0)
		goto ivib_case_0;

	eax= 0x80;
	if (edx == 0)
		goto ivib_case_1;

	edx--;
	if (edx == 0)
		goto ivib_case_2;

	edx = 0x80;
	edx -= *ivibposptr;
	checkmem();
//trim:
	do
	{
		eax -= edx;
		eax >>= 1;
		// delta = (128 - ((384 - cptr->ivibpos) & 0xFF)) >> 1 (case 3)
		// delta = (128 - ((cptr->ivibpos + 128) & 0xFF)) >> 1 (case 2)
		goto ivib_end_switch;
ivib_case_2:
		edx = *ivibposptr;
		edx -= eax;
	} while (1);

ivib_case_1:
	edx -= edx;//todo sbb
	edx &= eax;
	eax = edx - 0x40; // delta = +/- 64
	goto ivib_end_switch;

ivib_case_0:
	ecx = *ivibposptr;
	edx = *ivibposptr;
	ecx &= 0x7f;
	eax += -1 + (ecx<65);
	ecx ^= eax;
	eax &= 0x81;
	ecx += eax;
	int B = !!(edx & 128);
	edx >>= 8;
	//bug compatibility with ufmod.asm 1.25.2a
	//sometimes this reads offset 0x40 (when ufchan->ivibpos == 0xC0)
	//reading 0xAB which is the first byte of constant f0_0833
	assert(ecx < 0x41);
	eax = sin64[ecx];
	edx -= edx + B;
	eax ^= edx;
	eax -= edx;

ivib_end_switch:
	
	checkmem();

	edx = instrument->iVIBdepth;
	edx *= eax;
	eax = instrument->VIBsweep;
	if (eax != 0)
	{
		int edi=eax;
		eax = ufchan->ivibsweeppos;
		int64_t tmp = (int64_t)eax * edx;
		eax = (int)(tmp / edi);
		edx = (int)(tmp % edi);
		tmp_3 = eax;
		eax = edx;
		edx = tmp_3;
		tmp_3 = eax;
		eax = edi;
		edi = tmp_3;
	}

//sweep_ok:
	edx >>= 6;
	ufchan->freqdelta += edx;
	edx = ufchan->ivibsweeppos;
	edx++;
	if (edx > eax)
	{
		tmp_3 = eax;
		eax = edx;
		edx = tmp_3;
	}

//sweeppos_ok:
	checkmem();
	{
		eax = instrument->VIBrate;
		eax += *ivibposptr;
		ufchan->ivibsweeppos = edx;
		ufchan->notectrl |= FMUSIC_FREQ;
		if (eax&0xff00)//todo
			eax -= 256;
		*ivibposptr = eax;

		FSOUND_CHANNEL *channel=ufchan->cptr;

		if ((ufchan->notectrl & FMUSIC_TRIGGER) != 0)
		{
			if (channel->fsptr != NULL)
			{
				FSOUND_CHANNEL *altchannel;
				uintptr_t ecx;

				// Swap between channels to avoid sounds cutting each other off and causing a click
				ecx = (uintptr_t)channel;
				ecx -= (uintptr_t)_mod.module.Channels;
				//ecx is offset of channels

				//if we're on the odd numbered one, go to the even one, else go to the odd one
				if ((ecx / FSOUND_CHANNEL_size)&1)
					altchannel = channel - 1;
				else
					altchannel = channel + 1;
				
				checkmem();

				// Copy the whole channel except its trailing data
				ufchan->cptr = altchannel;

				ecx = offsetof(FSOUND_CHANNEL, fsptr);//  (FSOUND_CHANNEL_size - 20)/4
				ufmemcpy(altchannel, channel, ecx);

				checkmem();

				// This should cause the old channel to ramp out nicely
				channel->actualvolume = 0;
				channel->leftvolume = 0;
				channel->rightvolume = 0;
				channel = altchannel;
			}
//no_swap:
			{
				channel->fsptr = flags_sptr;

				// START THE SOUND!

				channel->mixposlo = 0;
				channel->ramp_leftvolume = 0;
				channel->ramp_rightvolume = 0;
				channel->ramp_count = 0;
				channel->speeddir = 0;
				channel->mixpos = channel->fsampleoffset;
				channel->fsampleoffset = 0;

				checkmem();
			}
		}
//no_trig:

		{
			int edi;
			unsigned int ecx = 255;

			if ((ufchan->notectrl & FMUSIC_VOLUME) == 0)
				goto no_volume;

			eax = ufchan->volume;
			eax += ufchan->voldelta;
			edx = eax;
			eax <<= 8;
			eax -= edx;
			int64_t tmp = (int64_t)eax * (int)_mod.module.globalvolume;
			eax = (unsigned int)tmp;
			edx = tmp >> 32;
			edx = ~edx;
			eax &= edx;
			if (eax > 0xff000)
				eax = 0xff000;

//vol_upper_bound_ok:;
			tmp = (int64_t)eax * ufchan->envvolfrac.val;
			tmp = tmp * ufchan->fadeoutvol;
			edx = tmp >> 32;
			edx >>= 3;
			eax = channel->actualpan;
			channel->actualvolume = edx;

			edi = edx;
			tmp = (int64_t)eax * edx;
			eax = (int)(tmp / ecx);
			channel->leftvolume = eax;

			eax = ecx;
			eax -= channel->actualpan;
			tmp = (int64_t)eax * edi;
			eax = (int)(tmp / ecx);
			channel->rightvolume = eax;
no_volume:
			if ((ufchan->notectrl & FMUSIC_PAN) != 0)
			{
				edi = 0x80;
				eax = ufchan->pan;
				eax -= edi;
				edx = cdq(eax);
				eax ^= edx;
				eax -= edx;
				edi -= eax;
				eax = ufchan->envpanfrac.val;
				edi >>= 5;
				eax -= 0x20;
				eax = eax * edi;
				eax += ufchan->pan;
				edx = cdq(eax);
				edx = ~edx;
				eax &= edx;
				edi = channel->actualvolume;
				edx = ecx;
				tmp_4 = eax;
				eax = edi;
				edi = tmp_4;
				if (edi < ecx)
				{
					int64_t tmp = (int64_t)eax * edi;
					eax = (int)(tmp / ecx);
					edx = edi;
				}
//pan_ae255:
				tmp_5 = eax;
				eax = edx;
				edx = tmp_5;
				channel->actualpan = eax;
				channel->leftvolume = edx;
				eax ^= 0xff;
				int64_t tmp = (int64_t)eax * channel->actualvolume;
				eax = (int)(tmp / ecx);
				channel->rightvolume = eax;
			}
//no_pan:
			checkmem();

			if ((ufchan->notectrl & FMUSIC_FREQ) != 0)
			{
				ecx = ufchan->freq;
				edx = 0;
				ecx += ufchan->freqdelta;
				eax = edx + 0x28;// f = 40 Hz
				if (ecx > 0)
				{
					if ((_mod.module.flags & 1) == 0)
					{
						eax = 0xda7790 / ecx;//14317456
					}
					else
					{
//modflags_n1:;
						int tmp;
						tmp = 0x1200;
						tmp -= ufchan->freq;
						tmp += ufchan->freqdelta;

						/*
						__asm ("fild dword [tmp]");
						__asm ("fmul dword [f0_0013]");
						__asm ("fld st(0)");
						__asm ("frndint");
						__asm ("fsub st(1), st(0)");
						__asm ("fxch st(1)");
						__asm ("f2xm1");
						__asm ("fld1");
						__asm ("faddp st(1)");
						__asm ("fscale");
						__asm ("fstp st(1)");
						__asm ("fmul dword [f8363_0]");
						__asm ("fistp dword [tmp]");
						*/

						/*
						multiply by 0.00133
						separate fraction and int parts of number
						add the powers of 2 of each part
						multiply by 8363.0
						return (int)(f8363_0 * powf(2.0f, eax * f0_0013));
						*/
						//int ans = (int)(f8363_0 * powf(2.0f, (float)tmp * f0_0013));
#ifdef INLINE_X86

#ifdef USE_TABLE_HZ
						eax = pow2_table_DoFlags(tmp);
#endif
						__asm {
							fild [tmp]
							fmul [f0_0013]
							fld st(0)
							frndint
							fsub st(1), st(0)
							fxch st(1)
							f2xm1
							fld1
							faddp st(1),st(0)
							fscale
							fstp st(1)
							fmul [f8363_0]
							fistp [tmp]
						};

#ifdef USE_TABLE_HZ
						assert(eax == tmp);
#endif

#else

#ifdef USE_TABLE_HZ
						tmp = pow2_table_DoFlags(tmp);
#else
						tmp = (int)(0.5f+f8363_0 * pow2f((float)tmp * f0_0013));
#endif

#endif
						eax = tmp;
					}
				}
				checkmem();
//freq_bounds_ok:
				int tmp;
				int64_t tmp64;
				tmp = eax / FSOUND_MixRate;
				tmp64 = (int64_t)(eax % FSOUND_MixRate)<<32;
				channel->speedhi = tmp;

				eax = (INT)(tmp64 / FSOUND_MixRate);
				channel->speedlo = eax;
//no_freq:;
				checkmem();
			}
		}
	}
	checkmem();
}

BOOL WINAPI _DllMainCRTStartup(HINSTANCE hinstDLL,	DWORD fdwReason,LPVOID lpReserved)
{
	//switch (fdwReason)
	//{
	//	case DLL_PROCESS_ATTACH: break;
	//	case DLL_THREAD_ATTACH: break;
	//	case DLL_THREAD_DETACH: break;
	//	case DLL_PROCESS_DETACH: break;
	//}
	return TRUE;
}