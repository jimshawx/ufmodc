#include "config.h"
#include "ufmodint.h"

typedef enum
{
	DUMPT_CONSOLE,
	DUMPT_FILE,
} dumptype;

#ifdef USE_DUMP

#ifdef __cplusplus
extern "C" {
#endif

	extern void dumpstate(const char *when, uF_MOD_STATE *_mod);
	extern void dumpmodule(uF_MOD_STATE *_mod);
	extern void dumpset(dumptype t);
	extern void dumpbuffer(const char *name, void *ptr, unsigned int count, unsigned char bits);

#ifdef __cplusplus
}
#endif

#else
#define dumpstate(x,y)
#define dumpmodule(x)
#define dumpset(x)
#define dumpbuffer(w,x,y,z)
#endif

#ifdef USE_MEMCHECK
#define checkmem() _CrtCheckMemory()
#else
#define checkmem()
#endif