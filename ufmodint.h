#pragma once

#include <windows.h>

#define FSOUND_MixRate 48000
#define FREQ_40HZ_p 0x1B4E8
#define FREQ_40HZ_f 0x369D00

#define volumerampsteps 128
#define volumeramps_pow 7

#pragma pack(push)
#pragma pack(1)
/*
; Sample type - contains info on sample
uF_SAMP__length   EQU 0; sample length
uF_SAMP_loopstart EQU 4; loop start
uF_SAMP_looplen   EQU 8; loop length
uF_SAMP_defvol    EQU 12; default volume
uF_SAMP_finetune  EQU 13; finetune value from - 128 to 127
uF_SAMP_bytes     EQU 14; type[b 0 - 1] : 0 - no loop
;                1 - forward loop
;                2 - bidirectional loop(aka ping - pong)
; [b 4] : 0 - 8 - bit sample data
;                1 - 16 - bit sample data
uF_SAMP_defpan    EQU 15; default pan value from 0 to 255
uF_SAMP_relative  EQU 16; relative note(signed value)
uF_SAMP_Resved    EQU 17; reserved, known values : 00h - regular delta packed sample data
;                         ADh - ModPlug 4 - bit ADPCM packed sample data
uF_SAMP_loopmode  EQU 18
uF_SAMP__align    EQU 19
uF_SAMP_buff      EQU 20; sound data
uF_SAMP_size      EQU 22
*/
typedef struct
{
	unsigned int _length; // sample length
	unsigned int loopstart; // loop start
	unsigned int looplen; // loop length
	unsigned char defvol; // default volume
	signed char finetune; // finetune value from - 128 to 127
	unsigned char bytes;// type[b 0 - 1] : 0 - no loop
		//                1 - forward loop
		//                2 - bidirectional loop(aka ping - pong)
		// [b 4] : 0 - 8 - bit sample data
		//                1 - 16 - bit sample data
	unsigned char defpan; // default pan value from 0 to 255
	signed char relative; // relative note(signed value)
	unsigned char Resved; // reserved, known values : 00h - regular delta packed sample data
		//                         ADh - ModPlug 4 - bit ADPCM packed sample data
	unsigned char loopmode;
	unsigned char _align;
	unsigned short buff; // sound data marker
} uF_SAMP;
//#define uF_SAMP_size (offsetof(uF_SAMP, buff))
#define uF_SAMP_size (sizeof(uF_SAMP))

/*
; Channel type - contains information on a mixing channel
FSOUND_CHANNEL_actualvolume     EQU 0; volume
FSOUND_CHANNEL_actualpan        EQU 4; panning value
FSOUND_CHANNEL_fsampleoffset    EQU 8; sample offset(sample starts playing from here)
FSOUND_CHANNEL_leftvolume       EQU 12; adjusted volume for left channel(panning involved)
FSOUND_CHANNEL_rightvolume      EQU 16; adjusted volume for right channel(panning involved)
FSOUND_CHANNEL_mixpos           EQU 20; high part of 32:32 fractional position in sample
FSOUND_CHANNEL_speedlo          EQU 24; playback rate - low part fractional
FSOUND_CHANNEL_speedhi          EQU 28; playback rate - high part fractional
FSOUND_CHANNEL_ramp_LR_target   EQU 32
FSOUND_CHANNEL_ramp_leftspeed   EQU 36
FSOUND_CHANNEL_ramp_rightspeed  EQU 40
FSOUND_CHANNEL_fsptr            EQU 44; pointer to FSOUND_SAMPLE currently playing sample
FSOUND_CHANNEL_mixposlo         EQU 48; low part of 32:32 fractional position in sample
FSOUND_CHANNEL_ramp_leftvolume  EQU 52
FSOUND_CHANNEL_ramp_rightvolume EQU 56
FSOUND_CHANNEL_ramp_count       EQU 60
FSOUND_CHANNEL_speeddir         EQU 62; playback direction - forwards or backwards
FSOUND_CHANNEL_size             EQU 64
*/
typedef struct
{
	unsigned int actualvolume; // volume
	unsigned int actualpan; // panning value
	unsigned int fsampleoffset; // sample offset(sample starts playing from here)
	unsigned int leftvolume; // adjusted volume for left channel(panning involved)
	unsigned int rightvolume; // adjusted volume for right channel(panning involved)
	unsigned int mixpos; // high part of 32:32 fractional position in sample
	unsigned int speedlo; // playback rate - low part fractional
	int speedhi; // playback rate - high part fractional
	unsigned int ramp_LR_target;
	int ramp_leftspeed;
	int ramp_rightspeed;
	uF_SAMP *fsptr; // pointer to FSOUND_SAMPLE currently playing sample
	unsigned int mixposlo; // low part of 32:32 fractional position in sample
	unsigned int ramp_leftvolume;
	unsigned int ramp_rightvolume;
	unsigned short ramp_count;
	unsigned short speeddir; // playback direction - 0 forwards or 1 backwards
} FSOUND_CHANNEL;
#define FSOUND_CHANNEL_size (sizeof(FSOUND_CHANNEL))

/*
; Single note type - contains info on 1 note in a pattern
uF_NOTE_unote   EQU 0; note to play at(0 - 97) 97 = keyoff
uF_NOTE_number  EQU 1; sample being played(0 - 128)
uF_NOTE_uvolume EQU 2; volume column value(0 - 64)  255 = no volume
uF_NOTE_effect  EQU 3; effect number(0 - 1Ah)
uF_NOTE_eparam  EQU 4; effect parameter(0 - 255)
uF_NOTE_size    EQU 5
*/
typedef struct
{
	unsigned char unote; // note to play at(0 - 97) 97 = keyoff
	unsigned char number; // sample being played(0 - 128)
	unsigned char uvolume; // volume column value(0 - 64)  255 = no volume
	unsigned char effect; // effect number(0 - 1Ah)
	unsigned char eparam; // effect parameter(0 - 255)
} uF_NOTE;
#define uF_NOTE_size (sizeof(uF_NOTE))

/*
uF_STATS_L_vol   EQU 0; L channel RMS volume
uF_STATS_R_vol   EQU 2; R channel RMS volume
uF_STATS_s_row   EQU 4
uF_STATS_s_order EQU 6
uF_STATS_size    EQU 8
*/
typedef struct
{
	unsigned short L_vol; // L channel RMS volume
	unsigned short R_vol; // R channel RMS volume
	unsigned short s_row;
	unsigned short s_order;
} uF_STATS;
#define uF_STATS_size (sizeof(uF_STATS))

/*
; Pattern data type
uF_PAT_rows        EQU 0
uF_PAT_patternsize EQU 2
uF_PAT_data        EQU 4; pointer to FMUSIC_NOTE
uF_PAT_size        EQU 8
*/
typedef struct
{
	short rows;
	unsigned short patternsize;
	uF_NOTE *data; // pointer to FMUSIC_NOTE
} uF_PAT;
#define uF_PAT_size (sizeof(uF_PAT))

/*
; Multi sample extended instrument
uF_INST_sample       EQU 0; 16 pointers to FSOUND_SAMPLE per instrument
uF_INST_keymap       EQU 64; sample keymap assignments
uF_INST_VOLPoints    EQU 160; volume envelope points
uF_INST_PANPoints    EQU 208; panning envelope points
uF_INST_VOLnumpoints EQU 256; number of volume envelope points
uF_INST_PANnumpoints EQU 257; number of panning envelope points
uF_INST_VOLsustain   EQU 258; volume sustain point
uF_INST_VOLLoopStart EQU 259; volume envelope loop start
uF_INST_VOLLoopEnd   EQU 260; volume envelope loop end
uF_INST_PANsustain   EQU 261; panning sustain point
uF_INST_PANLoopStart EQU 262; panning envelope loop start
uF_INST_PANLoopEnd   EQU 263; panning envelope loop end
uF_INST_VOLtype      EQU 264; type of envelope, bit 0:On 1 : Sustain 2 : Loop
uF_INST_PANtype      EQU 265; type of envelope, bit 0:On 1 : Sustain 2 : Loop
uF_INST_VIBtype      EQU 266; instrument vibrato type
uF_INST_VIBsweep     EQU 267; time it takes for vibrato to fully kick in
uF_INST_iVIBdepth    EQU 268; depth of vibrato
uF_INST_VIBrate      EQU 269; rate of vibrato
uF_INST_VOLfade      EQU 270; fade out value
uF_INST_size         EQU 272
*/
typedef struct
{
	uF_SAMP *sample[16]; // 16 pointers to FSOUND_SAMPLE per instrument
	unsigned char keymap[96]; // sample keymap assignments
	unsigned short VOLPoints[24]; // volume envelope points
	unsigned short PANPoints[24]; // panning envelope points
	unsigned char VOLnumpoints; // number of volume envelope points
	unsigned char PANnumpoints; // number of panning envelope points
	unsigned char VOLsustain; // volume sustain point
	unsigned char VOLLoopStart; // volume envelope loop start
	unsigned char VOLLoopEnd; // volume envelope loop end
	unsigned char PANsustain; // panning sustain point
	unsigned char PANLoopStart; // panning envelope loop start
	unsigned char PANLoopEnd; // panning envelope loop end
	unsigned char VOLtype; // type of envelope, bit 0:On 1 : Sustain 2 : Loop
	unsigned char PANtype; // type of envelope, bit 0:On 1 : Sustain 2 : Loop
	unsigned char VIBtype; // instrument vibrato type
	unsigned char VIBsweep; // time it takes for vibrato to fully kick in
	unsigned char iVIBdepth; // depth of vibrato
	unsigned char VIBrate; // rate of vibrato
	unsigned short VOLfade; // fade out value
} uF_INST;
#define uF_INST_size (sizeof(uF_INST))

/*
; Channel type - contains information on a mod channel
uF_CHAN_note          EQU 0; last note set in channel
uF_CHAN_samp          EQU 1; last sample set in channel
uF_CHAN_notectrl      EQU 2; flags for DoFlags proc
uF_CHAN_inst          EQU 3; last instrument set in channel
uF_CHAN_cptr          EQU 4; pointer to FSOUND_CHANNEL system mixing channel
uF_CHAN_freq          EQU 8; current mod frequency period for this channel
uF_CHAN_volume        EQU 12; current mod volume for this channel
uF_CHAN_voldelta      EQU 16; delta for volume commands : tremolo / tremor, etc
uF_CHAN_freqdelta     EQU 20; delta for frequency commands : vibrato / arpeggio, etc
uF_CHAN_pan           EQU 24; current mod pan for this channel
uF_CHAN_envvoltick    EQU 28; tick counter for envelope position
uF_CHAN_envvolpos     EQU 32; envelope position
uF_CHAN_envvoldelta   EQU 36; delta step between points
uF_CHAN_envpantick    EQU 40; tick counter for envelope position
uF_CHAN_envpanpos     EQU 44; envelope position
uF_CHAN_envpandelta   EQU 48; delta step between points
uF_CHAN_ivibsweeppos  EQU 52; instrument vibrato sweep position
uF_CHAN_ivibpos       EQU 56; instrument vibrato position
uF_CHAN_keyoff        EQU 60; flag whether keyoff has been hit or not
uF_CHAN_envvolstopped EQU 62; flag to say whether envelope has finished or not
uF_CHAN_envpanstopped EQU 63; flag to say whether envelope has finished or not
uF_CHAN_envvolfrac    EQU 64; fractional interpolated envelope volume
uF_CHAN_envvol        EQU 68; final interpolated envelope volume
uF_CHAN_fadeoutvol    EQU 72; volume fade out
uF_CHAN_envpanfrac    EQU 76; fractional interpolated envelope pan
uF_CHAN_envpan        EQU 80; final interpolated envelope pan
uF_CHAN_period        EQU 84; last period set in channel
uF_CHAN_sampleoffset  EQU 88; sample offset for this channel in SAMPLES
uF_CHAN_portatarget   EQU 92; note to porta to
uF_CHAN_patloopno     EQU 96; pattern loop variables for effect E6x
uF_CHAN_patlooprow    EQU 100
uF_CHAN_realnote      EQU 104; last realnote set in channel
uF_CHAN_recenteffect  EQU 105; previous row's effect. used to correct tremolo volume
uF_CHAN_portaupdown   EQU 106; last porta up / down value
; uF_CHAN_unused       EQU 107
uF_CHAN_xtraportadown EQU 108; last porta down value
uF_CHAN_xtraportaup   EQU 109; last porta up value
uF_CHAN_volslide      EQU 110; last volume slide value
uF_CHAN_panslide      EQU 111; pan slide parameter
uF_CHAN_retrigx       EQU 112; last retrig volume slide used
uF_CHAN_retrigy       EQU 113; last retrig tick count used
uF_CHAN_portaspeed    EQU 114; porta speed
uF_CHAN_vibpos        EQU 115; vibrato position
uF_CHAN_vibspeed      EQU 116; vibrato speed
uF_CHAN_vibdepth      EQU 117; vibrato depth
uF_CHAN_tremolopos    EQU 118; tremolo position
uF_CHAN_tremolospeed  EQU 119; tremolo speed
uF_CHAN_tremolodepth  EQU 120; tremolo depth
uF_CHAN_tremorpos     EQU 121; tremor position
uF_CHAN_tremoron      EQU 122; remembered parameters for tremor
uF_CHAN_tremoroff     EQU 123; remembered parameters for tremor
uF_CHAN_wavecontrol   EQU 124; waveform type for vibrato andtremolo(4bits each)
uF_CHAN_finevslup     EQU 125; parameter for fine volume slide down
uF_CHAN_fineportaup   EQU 126; parameter for fine porta slide up
uF_CHAN_fineportadown EQU 127; parameter for fine porta slide down
uF_CHAN_size          EQU 128
*/
typedef struct
{
	unsigned int tick;
	unsigned int pos;
	unsigned int delta;
} uF_VOLPAN;
typedef struct
{
	int frac;
	int val;
} uF_VOLPANFRAC;
typedef struct
{
	unsigned char note; // last note set in channel
	unsigned char samp; // last sample set in channel
	unsigned char notectrl; // flags for DoFlags proc
	unsigned char inst; // last instrument set in channel
	FSOUND_CHANNEL *cptr; // pointer to FSOUND_CHANNEL system mixing channel
	unsigned int freq; // current mod frequency period for this channel
	int volume; // current mod volume for this channel
	int voldelta; // delta for volume commands : tremolo / tremor, etc
	int freqdelta; // delta for frequency commands : vibrato / arpeggio, etc
	unsigned int pan; // current mod pan for this channel
	/*
	unsigned int envvoltick; // tick counter for envelope position
	unsigned int envvolpos; // envelope position
	unsigned int envvoldelta; // delta step between points
	unsigned int envpantick; // tick counter for envelope position
	unsigned int envpanpos; // envelope position
	unsigned int envpandelta; // delta step between points
	*/
	uF_VOLPAN envvol;
	uF_VOLPAN envpan;
	unsigned int ivibsweeppos; // instrument vibrato sweep position
	unsigned int ivibpos; // instrument vibrato position
	unsigned short keyoff; // flag whether keyoff has been hit or not
	unsigned char envvolstopped; // flag to say whether envelope has finished or not
	unsigned char envpanstopped; // flag to say whether envelope has finished or not
	/*
	unsigned int envvolfrac; // fractional interpolated envelope volume
	unsigned int envvol; // final interpolated envelope volume
	*/
	uF_VOLPANFRAC envvolfrac;
	int fadeoutvol; // volume fade out
	/*
	unsigned int envpanfrac; // fractional interpolated envelope pan
	unsigned int envpan; // final interpolated envelope pan
	*/
	uF_VOLPANFRAC envpanfrac;
	unsigned int period; // last period set in channel
	unsigned int sampleoffset; // sample offset for this channel in SAMPLES
	unsigned int portatarget; // note to porta to
	unsigned int patloopno; // pattern loop variables for effect E6x
	int patlooprow;
	unsigned char realnote; // last realnote set in channel
	unsigned char recenteffect; // previous row's effect. used to correct tremolo volume
	signed char portaupdown; // last porta up / down value
	unsigned char unused;
	unsigned char xtraportadown; // last porta down value
	unsigned char xtraportaup; // last porta up value
	unsigned char volslide; // last volume slide value
	unsigned char panslide; // pan slide parameter
	unsigned char retrigx; // last retrig volume slide used
	unsigned char retrigy; // last retrig tick count used
	unsigned char portaspeed; // porta speed
	unsigned char vibpos; // vibrato position
	unsigned char vibspeed; // vibrato speed
	signed char vibdepth; // vibrato depth
	unsigned char tremolopos; // tremolo position
	unsigned char tremolospeed; // tremolo speed
	signed char tremolodepth; // tremolo depth
	unsigned char tremorpos; // tremor position
	unsigned char tremoron; // remembered parameters for tremor
	unsigned char tremoroff; // remembered parameters for tremor
	unsigned char wavecontrol; // waveform type for vibrato andtremolo(4bits each)
	signed char finevslup; // parameter for fine volume slide down
	unsigned char fineportaup; // parameter for fine porta slide up
	unsigned char fineportadown; // parameter for fine porta slide down
} uF_CHAN;
#define uF_CHAN_size (sizeof(uF_CHAN))

/*
; Song type - contains info on song
uF_MOD_pattern              EQU 0; pointer to FMUSIC_PATTERN array for this song
uF_MOD_instrument           EQU 4; pointer to FMUSIC_INSTRUMENT array for this song
uF_MOD_mixer_samplesleft    EQU 8
uF_MOD_globalvolume         EQU 12; global mod volume
uF_MOD_tick                 EQU 16; current mod tick
uF_MOD_speed                EQU 20; speed of song in ticks per row
uF_MOD_order                EQU 24; current song order position
uF_MOD_row                  EQU 28; current row in pattern
uF_MOD_patterndelay         EQU 32; pattern delay counter
uF_MOD_nextorder            EQU 36; current song order position
uF_MOD_nextrow              EQU 40; current row in pattern
uF_MOD_unused1              EQU 44
uF_MOD_numchannels          EQU 48; number of channels
uF_MOD_Channels             EQU 52; channel pool
uF_MOD_uFMOD_Ch             EQU 56; channel array for this song
uF_MOD_mixer_samplespertick EQU 60
uF_MOD_numorders            EQU 64; number of orders(song length)
uF_MOD_restart              EQU 66; restart position
uF_MOD_numchannels_xm       EQU 68
uF_MOD_globalvsl            EQU 69; global mod volume
uF_MOD_numpatternsmem       EQU 70; number of allocated patterns
uF_MOD_numinsts             EQU 72; number of instruments
uF_MOD_flags                EQU 74; flags such as linear frequency, format specific quirks, etc
uF_MOD_defaultspeed         EQU 76
uF_MOD_defaultbpm           EQU 78
uF_MOD_orderlist            EQU 80; pattern playing order list
uF_MOD_size                 EQU 336
*/
typedef struct
{
	uF_PAT *pattern; // pointer to FMUSIC_PATTERN array for this song 
	uF_INST *instrument; // pointer to FMUSIC_INSTRUMENT array for this song
	unsigned int mixer_samplesleft;
	unsigned int globalvolume; // global mod volume
	unsigned int tick; // current mod tick
	unsigned int speed; // speed of song in ticks per row
	int order; // current song order position
	int row; // current row in pattern
	unsigned int patterndelay; // pattern delay counter
	int nextorder; // current song order position
	int nextrow; // current row in pattern
	unsigned int unused1;
	unsigned int numchannels; // number of channels
	FSOUND_CHANNEL *Channels; // channel pool
	uF_CHAN *uFMOD_Ch; // channel array for this song

	union
	{
		unsigned int mixer_samplespertick; //overlays XM module header size
		unsigned int header_size;
	};
	short numorders; // number of orders(song length)
	unsigned short restart; // restart position
	unsigned char numchannels_xm;
	unsigned char globalvsl; // global mod volume // numchannels_xm is usually a word, this has been jammed in between since numchannels is <= 32
	unsigned short numpatternsmem; // number of allocated patterns
	unsigned short numinsts; // number of instruments
	unsigned short flags; // flags such as linear frequency, format specific quirks, etc
	unsigned short defaultspeed;
	unsigned short defaultbpm;
	unsigned char orderlist[256]; // pattern playing order list
} uF_MOD;
#define uF_MOD_size (sizeof(uF_MOD))

#define XM_MEMORY                  1
#define XM_FILE                    2
#define XM_NOLOOP                  8
#define XM_SUSPENDED               16
#define XM_INTEGRATION_TEST        32

#define FMUSIC_ENVELOPE_SUSTAIN    2
#define FMUSIC_ENVELOPE_LOOP       4
#define FMUSIC_FREQ                1
#define FMUSIC_VOLUME              2
#define FMUSIC_PAN                 4
#define FMUSIC_TRIGGER             8
#define FMUSIC_VOLUME_OR_FREQ      3
#define FMUSIC_VOLUME_OR_PAN       6
#define FMUSIC_VOL_OR_FREQ_OR_TR   11
#define FMUSIC_FREQ_OR_TRIGGER     9
#define NOT_FMUSIC_TRIGGER         0xF7
#define NOT_FMUSIC_TRIGGER_OR_FRQ  0xF6

enum FMUSIC_XMCOMMANDS
{
	FMUSIC_XM_PORTAUP = 1,
	FMUSIC_XM_PORTADOWN = 2,
	FMUSIC_XM_PORTATO = 3,
	FMUSIC_XM_VIBRATO = 4,
	FMUSIC_XM_PORTATOVOLSLIDE = 5,
	FMUSIC_XM_VIBRATOVOLSLIDE = 6,
	FMUSIC_XM_TREMOLO = 7,
	FMUSIC_XM_SETPANPOSITION = 8,
	FMUSIC_XM_SETSAMPLEOFFSET = 9,
	FMUSIC_XM_VOLUMESLIDE = 10,
	FMUSIC_XM_PATTERNJUMP = 11,
	FMUSIC_XM_SETVOLUME = 12,
	FMUSIC_XM_PATTERNBREAK = 13,
	FMUSIC_XM_SPECIAL = 14,
	FMUSIC_XM_SETSPEED = 15,
	FMUSIC_XM_SETGLOBALVOLUME = 16,
	FMUSIC_XM_GLOBALVOLSLIDE = 17,
	FMUSIC_XM_KEYOFF = 20,
	FMUSIC_XM_PANSLIDE = 25,
	FMUSIC_XM_MULTIRETRIG = 27,
	FMUSIC_XM_TREMOR = 29,
	FMUSIC_XM_EXTRAFINEPORTA = 33
};

enum FMUSIC_XMCOMMANDSSPECIAL
{
	FMUSIC_XM_FINEPORTAUP = 1,
	FMUSIC_XM_FINEPORTADOWN = 2,
	FMUSIC_XM_SETGLISSANDO = 3,
	FMUSIC_XM_SETVIBRATOWAVE = 4,
	FMUSIC_XM_SETFINETUNE = 5,
	FMUSIC_XM_PATTERNLOOP = 6,
	FMUSIC_XM_SETTREMOLOWAVE = 7,
	FMUSIC_XM_SETPANPOSITION16 = 8,
	FMUSIC_XM_RETRIG = 9,
	FMUSIC_XM_NOTECUT = 12,
	FMUSIC_XM_NOTEDELAY = 13,
	FMUSIC_XM_PATTERNDELAY = 14
};

#define FSOUND_Block 10
#define fragmentsmask 0xF
#define FSOUND_BlockSize  (1 << FSOUND_Block)
#define totalblocks       (fragmentsmask + 1)
#define FSOUND_BufferSize ((FSOUND_BlockSize) * totalblocks)

#define uF_WMMBlock WAVEHDR
#define uF_WMMBlock_size (sizeof(uF_WMMBlock))

typedef struct
{
	char idtext[17];
	char modulename[20];
	char oneA;
	char trackername[20];
	unsigned short version;
} uF_XM;
#define uF_XM_size (sizeof(uF_XM))

typedef struct
{
	union
	{
		unsigned int loadxm_pat;
		unsigned int headersize;
	};
	unsigned char packingtype;
	unsigned short rowcount;
	unsigned short loadxm_pat_size;
} uF_PATTERN_HEADER;
#define uF_PATTERN_HEADER_size (sizeof(uF_PATTERN_HEADER))

typedef struct
{
	unsigned int headersize;
	char instrumentname[22];
	unsigned char instrumenttype;
	unsigned short samplecount;
	unsigned int sampleheadersize;
} uF_INSTRUMENT_HEADER;
#define uF_INSTRUMENT_HEADER_size (sizeof(uF_INSTRUMENT_HEADER))

typedef struct
{
	unsigned int loadxm_sample_2;//loadxm_samplelen
	unsigned int loadxm_s0loopstart;
	unsigned int loadxm_s0looplen;
	unsigned char volume;
	unsigned char finetune;
	unsigned char loadxm_s0bytes[4];//type, pan, relativenoteno, reserved
	union
	{
		unsigned char loadxm_s0loopmode;
		char samplename[22];
	};
} uF_SAMPLE_HEADER;

#define SAMPLE_LOOP_MASK 3
#define SAMPLE_LOOP_TYPE_NONE 0
#define SAMPLE_LOOP_TYPE_FORWARD 1
#define SAMPLE_LOOP_TYPE_PINGPONG 3


#define MODPLUG_4BIT_ADPCM 0xAD

typedef struct
{
	uF_MOD module;

	unsigned int mmt[3];//todo, remove me
	HANDLE hHeap;
	HANDLE hThread;
	HWAVEOUT hWaveOut;
	unsigned int uFMOD_FillBlk;
	HANDLE SW_Exit;// file ptr
	// mix buffer memory block (align 16!)
	unsigned int MixBuf[FSOUND_BlockSize * 2];
	unsigned char ufmod_noloop; // enable/disable restart loop
	unsigned char ufmod_pause; // pause/resume
	unsigned char mix_endflag[2];
	unsigned int mmf_module_size; // module size
	unsigned int mmf_module_posn; // module position
	union
	{
		HGLOBAL mmf_module_resource; // resource handle
		char *mmf_module_address;    // module address
	};
	HANDLE mmf_module_mapping; //file mapping
	unsigned int mmf_pad;//todo, remove me
	unsigned int ufmod_vol; // global volume scale
	void (*uFMOD_fopen)(void *esi_name);
	void (*uFMOD_fread)(void *eax_buffer, unsigned int edx_size);
	void (*uFMOD_fclose)(void);
	unsigned int unused; //unused (align 16)
	unsigned short databuf[FSOUND_BufferSize*2];
	uF_WMMBlock MixBlock;
	unsigned char RealBlock[4];
	unsigned int time_ms;
	uF_STATS tInfo[totalblocks];
	char szTtl[24];// track title
	uF_SAMP DummySamp;

	//added by JS
	HINSTANCE hInstance;

	int ramp_leftspeed;
	int ramp_rightspeed;
	unsigned int mmf_length;
	unsigned int mmf_mixcount;

	struct
	{
		uF_SAMP *donote_sptr;
		uF_INST *donote_iptr;
		unsigned char donote_jumpflag;
		struct {
			unsigned int donote_oldfreq;
			unsigned int donote_oldpan;
			int donote_currtick;
		} S2_C13;
	} S1;

	struct
	{
		uF_NOTE *doeff_current;
		uF_SAMP *sample;
	} S3;

} uF_MOD_STATE;

typedef signed long long int int64_t;
typedef unsigned long long int uint64_t;

#pragma pack(pop)
