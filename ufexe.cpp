// ufmod.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "config.h"

#include <windows.h>
#include "ufmod.h"

#include "ufmodint.h"
#include "diag.h"

#include <stdio.h>
#include <crtdbg.h>

static void integration_begin(const char *fname);
static void integration_end(void);

static void test();
static void genfreqtab();

static const char *tunes[] =
{
	"1.XM",
	"84556-blastoff.xm",
	"banana_boat_ii.xm",
	"beeper_-_epidemic.xm",
	"beepsampler.xm",
	"beep_beep.xm",
	"crazy_dreams.xm",
	"DEADLOCK.XM",
	//"deliflat.xm",
	"distant3.xm",
	"dpl_fast.xm",
	"grip_-_squarewave_stalker.xm",
	"heatwave.xm",
	"ice_dancer_20th.xm",
	"lhs_abr.xm",
	"lmt_dr_3.xm",
	"m.xm",
	"madness_final.xm",
	"megadreams.xm",
	"ml_siege.xm",
	"ni-piece.xm",
	"oler.xm",
	"pacman.xm",
	"pacman_dreams.xm",
	"pacmawax.xm",
	"pies.xm",
	"psirius_-_dawning_in_orion.xm",
	"purplesky_-_solar_winds.xm",
	"sawdance.xm",
	"squarewa.xm",
	"square_sounds_-_awe.xm",
	"surfonasinewave.xm",
	"transit.xm",
	//"vsn-beep.xm",
	NULL
};

int main(void)
{
	//test();
	//genfreqtab();

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

#ifdef INTEGRATION_TEST
	const char **t = tunes;
	while (*t)
	{
		char tmp[100];

		sprintf_s(tmp, 100, "../tests/evaluate/%s.wav", *t);
		integration_begin(tmp);

		sprintf_s(tmp, 100, "../tests/tunes/%s", *t);
		uFMOD_PlaySong((void *)tmp, 0, XM_FILE|XM_INTEGRATION_TEST);

		uFMOD_StopSong();
		
		integration_end();
		t++;
	}
#endif

#ifndef INTEGRATION_TEST
	const char **t = tunes;
	while (*t)
	{
		char tmp[100];
		sprintf_s(tmp, 100, "../tests/tunes/%s", *t);
		uFMOD_PlaySong((void *)tmp, 0, XM_FILE);
		printf("press Enter to continue...");
		//(void)getchar();
		while (!GetAsyncKeyState(VK_RETURN)) Sleep(100);
		uFMOD_StopSong();
		t++;
	}
#endif

	return 0;
}

static FILE *f = NULL;
static int recordmode = 0;
static unsigned int byteswritten = 0;
static void ensure()
{
	if (!f)
		fopen_s(&f, "trace2.txt", "w");
}

extern "C" void diagnostics(uF_MOD_STATE *mod)
{
	static int i;
	char tmp[20];

	sprintf_s(tmp, 20, "MIX %d", i);

	dumpset(DUMPT_FILE);

	if (!i)
	{
		system("del dump*.txt");
		system("del sample*.wav");
		dumpmodule(mod);
		dumpset(DUMPT_FILE);
	}

	//recordmode = i >= 2450 && i < 2600;

	if (i >= 0 && recordmode)
	{
		dumpstate(tmp, mod);
		dumpbuffer("mix", mod->MixBuf, FSOUND_BlockSize * 2, 32);
		dumpbuffer("data", mod->databuf, FSOUND_BufferSize * 2, 16);
	}

	ensure();
	sprintf_s(tmp, 20, "%05d\n", i);
	fputs(tmp, f);

	i++;
}

extern "C" void debug(unsigned int value)
{
	char tmp[100];
	static int i = 0;
	if (recordmode)
	{
		ensure();
		sprintf_s(tmp, 100, "%09d %08x %u\n", i, value, value);
		//OutputDebugString(tmp);
		fputs(tmp, f);
	}
	i++;
	//if (i == 76842)
	//	DebugBreak();
}

extern "C" void debugs(char *fmt,...)
{
	static char tmp[1000];

	static int i = 0;
	if (recordmode)
	{
		ensure();
		va_list ap;
		va_start(ap, fmt);
		vsprintf_s(tmp, 1000, fmt, ap);
		va_end(ap);
		OutputDebugString(tmp);
		fputs(tmp, f);
	}
	i++;
}

static FILE *intf;
static void integration_begin(const char *fname)
{
	unsigned int length;

	fopen_s(&intf, fname, "wb");
	if (!intf) return;

	static PCMWAVEFORMAT pcm =
	{
		WAVE_FORMAT_PCM,
		2,//channels
		FSOUND_MixRate,
		FSOUND_MixRate * 4,
		(2 * 16) / 8,//channels * bps / 8
		16//bps
	};

	fwrite("RIFF", 4, 1, intf);
	length = 0xffffffff;//todo?
	fwrite(&length, 4, 1, intf);
	fwrite("WAVE", 4, 1, intf);

	fwrite("fmt ", 4, 1, intf);
	length = sizeof(pcm);
	fwrite(&length, 4, 1, intf);
	fwrite(&pcm, sizeof(pcm), 1, intf);

	length = 0xffffffff;//todo?
	fwrite("data", 4, 1, intf);
	fwrite(&length, 4, 1, intf);

	byteswritten = ftell(intf);
}

extern "C" void integration(void *ptr, unsigned int count)
{
	fwrite(ptr, 2, count, intf);
	byteswritten += 2 * count;
	//if (byteswritten > 0x600000)
	//	recordmode = 1;
	//if (byteswritten > 0x620000)
	//	recordmode = 0;
}

static void integration_end(void)
{
	fclose(intf);
}

#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdint.h>

//https://stackoverflow.com/questions/47025373/fastest-implementation-of-the-natural-exponential-function-using-sse
float fastExp3(register float x)  // cubic spline approximation
{
	union { float f; int32_t i; } reinterpreter;

	reinterpreter.i = (int32_t)(12102203.0f * x) + 127 * (1 << 23);
	int32_t m = (reinterpreter.i >> 7) & 0xFFFF;  // copy mantissa
	// empirical values for small maximum relative error (8.34e-5):
	reinterpreter.i +=
		((((((((1277 * m) >> 14) + 14825) * m) >> 14) - 79749) * m) >> 11) - 626;
	return reinterpreter.f;
}

float fastExp4(register float x)  // quartic spline approximation
{
	union { float f; int32_t i; } reinterpreter;

	reinterpreter.i = (int32_t)(12102203.0f * x) + 127 * (1 << 23);
	int32_t m = (reinterpreter.i >> 7) & 0xFFFF;  // copy mantissa
	// empirical values for small maximum relative error (1.21e-5):
	reinterpreter.i += (((((((((((3537 * m) >> 16)
		+ 13668) * m) >> 18) + 15817) * m) >> 14) - 80470) * m) >> 11);
	return reinterpreter.f;
}

//https://www.musicdsp.org/en/latest/Other/222-fast-exp-approximations.html
float fastexp5(float x)
{
	return (120 + x * (120 + x * (60 + x * (20 + x * (5 + x))))) * 0.0083333333f;
}
float fastexp6(float x)
{
	return (720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888f;
}
inline float fastexp7(float x)
{
	return (5040 + x * (5040 + x * (2520 + x * (840 + x * (210 + x * (42 + x * (7 + x))))))) * 0.00019841269f;
}
float fastexp8(float x)
{
	return (40320 + x * (40320 + x * (20160 + x * (6720 + x * (1680 + x * (336 + x * (56 + x * (8 + x)))))))) * 2.4801587301e-5f;
}
float fastexp9(float x)
{
	return (362880 + x * (362880 + x * (181440 + x * (60480 + x * (15120 + x * (3024 + x * (504 + x * (72 + x * (9 + x))))))))) * 2.75573192e-6f;
}

float pow2f_1(float x)
{
	int i = (int)x;
	x -= i;
	x *= 0.69314718056f;
	return (1 << i) * (5040 + x * (5040 + x * (2520 + x * (840 + x * (210 + x * (42 + x * (7 + x))))))) * 0.00019841269f;
}

float pow2f(float x)
{
	int i = (int)x;
	//x -= i;
	//x *= 0.69314718056f;
	x = (x-(float)i) * 0.69314718056f;
	if (i >= 0)
		return ((720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888f)*(1<<i);
	else
		return ((720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888f)*(1.0f/(1<<-i));
}

double pow2d(double x)
{
	int i = (int)x;
	//x -= i;
	//x *= 0.69314718056f;
	x = (x-(double)i) * 0.69314718055994530941723212145818;//e^0.693 == 2, or ln 2 == 0.693
	if (i >= 0)
		return ((720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888)*(1<<i);
	else
		return ((720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888)*(1.0/(1<<-i));
}

float pow2f_3(float x)
{
	int i = (int)x;
	x -= i;
	x *= 0.69314718056f;
	return (1 << i) * (120 + x * (120 + x * (60 + x * (20 + x * (5 + x))))) * 0.0083333333f;
}

static void test()
{
#ifndef _WIN64
	static const float f0_0833 = 8.3333336e-2f;
	static const float f13_375 = 1.3375e1f;
	static const float f0_0013 = 1.302083375e-3f;
	static const float f8363_0 = 8.3630004275e3f;

	char msg[1024];
	int tmp, tmpc, tmpd, tmpe, tmpf, tmpg, tmpq, tmpr, tmps;
	float tmph;
	
	float loge2 = logf(2.0f);//0.69314718056

	//good up to 188, with off by 1, then 941, then
	for (int t = -4610; t < 4610; t++)
	{
		tmp = t;

		/*
		multiply by 0.00133
		separate fraction and int parts of number
		add the powers of 2 of each part
		multiply by 8363.0
		return (int)(f8363_0 * powf(2.0f, eax * f0_0013));
		*/

		__asm {
			fild[tmp]
			fmul[f0_0013]
			fld st(0)
			frndint
			fsub st(1), st(0)
			fxch st(1)
			f2xm1
			fld1
			faddp st(1), st(0)
			fscale
			fstp st(1)
			fmul[f8363_0]
			fistp[tmp]
		};

		tmph = f8363_0 * powf(2.0f, (float)t * f0_0013);
		tmpg = (int)(0.5f + (f8363_0 * powf(2.0f, (float)t * f0_0013)));
		tmpf = (int)(f8363_0 * powf(2.0f, (float)t * f0_0013));
		tmpe = (int)floorf(f8363_0 * powf(2.0f, (float)t * f0_0013));
		tmpd = (int)ceilf(f8363_0 * powf(2.0f, (float)t * f0_0013));
		tmpc = (int)roundf(f8363_0 * powf(2.0f, (float)t * f0_0013));

		tmpq = (int)(0.5f + f8363_0 * pow2f((float)t * f0_0013));
		tmps = (int)(0.5 + f8363_0 * pow2d((double)t * f0_0013));

		//this one, which uses double, gives exactly the same result as the assembler code.
		tmpr = (int)round(f8363_0 * pow(2.0, (double)t * f0_0013));

		if (tmp != tmps)
		{
			sprintf_s(msg, 1024, "%d %d %d %f %d %d %d\n", t, tmp, tmpc, (double)tmph, tmpq, tmpr, tmps);
		}
		else
		{
			sprintf_s(msg, 1024, "%d %d OK\n", t, tmp);
		}
		OutputDebugString(msg);
	}
	
#endif
}

static void genfreqtab()
{
#ifndef _WIN64
	FILE *c;
	static int w = 0;
	char txt[100];
	int tmp;
	const int mint0 = -4610;
	const int maxt0 = 4610;

	const int mint1 = 0;
	const int maxt1 = 300;

	char asserttxt0[100];
	//sprintf_s(asserttxt0, 100, "assert(c >= %d && c < %d);\nc-=%d;\n", mint0, maxt0, mint0);
	sprintf_s(asserttxt0, 100, "if(c < %d || c >= %d) DebugBreak();\n", mint0, maxt0);

	char asserttxt1[100];
	//sprintf_s(asserttxt1, 100, "assert(c >= %d && c < %d);\nc-=%d;\n", mint1, maxt1, mint1);
	sprintf_s(asserttxt1, 100, "if(c < %d || c >= %d) DebugBreak();\n", mint1, maxt1);

	int m;
	char minmax0[1000];
	m = 0;
	m += sprintf_s(minmax0 + m, 1000 - m, "static int minc = INT_MAX, maxc = INT_MIN;char tmp[100];\n");
	m += sprintf_s(minmax0 + m, 1000 - m, "if (c<minc){minc=c;sprintf_s(tmp,100,\"min0 %%d\\n\",c);OutputDebugString(tmp);}\n");
	m += sprintf_s(minmax0 + m, 1000 - m, "if (c>maxc){maxc=c;sprintf_s(tmp,100,\"max0 %%d\\n\",c);OutputDebugString(tmp);}\n");
	
	char minmax1[1000];
	m = 0;
	m += sprintf_s(minmax1 + m, 1000 - m, "static int minc = INT_MAX, maxc = INT_MIN; char tmp[100];\n");
	m += sprintf_s(minmax1 + m, 1000 - m, "if (c<minc){minc=c;sprintf_s(tmp,100,\"min1 %%d\\n\",c);OutputDebugString(tmp);}\n");
	m += sprintf_s(minmax1 + m, 1000 - m, "if (c>maxc){maxc=c;sprintf_s(tmp,100,\"max1 %%d\\n\",c);OutputDebugString(tmp);}\n");

	static const float f0_0833 = 8.3333336e-2f;
	static const float f13_375 = 1.3375e1f;
	static const float f0_0013 = 1.302083375e-3f;
	static const float f8363_0 = 8.3630004275e3f;

	fopen_s(&c, "freqtab.c", "w");
	if (!c)
		return;

	fputs("#include \"config.h\"\n", c);
	fputs("#ifdef USE_TABLE_HZ\n",c);

	fputs("\n", c);

	//fputs("#include <windows.h>\n", c);
	//fputs("#include <assert.h>\n", c);

	//fputs("#include <stdio.h>\n", c);
	//fputs("#include <limits.h>\n", c);

	fputs("\n", c);

	fputs("int pow2_table_DoFlags(int c)\n", c);
	fputs("{\n", c);
	fputs("static const int table[]={\n\t", c);

	w = 0;
	for (int t = mint0; t < maxt0; t++)
	{
		tmp = t;
		__asm {
			fild[tmp]
			fmul[f0_0013]
			fld st(0)
			frndint
			fsub st(1), st(0)
			fxch st(1)
			f2xm1
			fld1
			faddp st(1), st(0)
			fscale
			fstp st(1)
			fmul[f8363_0]
			fistp[tmp]
		};

		sprintf_s(txt, 100, "%d,", tmp);
		fputs(txt, c);
		w++;
		if (w % 16 == 0)
			fputs("\n\t", c);
	}
	fputs("};\n", c);
	//fputs(minmax0, c);
	//fputs(asserttxt0, c);
	fprintf(c, "\tc -= %d;\n", mint0);
	fputs("\treturn table[c];\n", c);
	fputs("}\n", c);

	fputs("\n",c);

	fputs("int pow2_table_AmigaPeriod(int c)\n", c);
	fputs("{\n", c);
	fputs("static const int table[]={\n\t", c);

	w = 0;
	for (int t = mint1; t < maxt1; t++)
	{
		tmp = t;
		__asm {
			fild[tmp]
			fmul[f0_0833]
			fld st(0)
			frndint
			fsub st(1), st(0)
			fxch st(1)
			f2xm1
			fld1
			faddp st(1), st(0)
			fscale
			fstp st(1)
			fmul[f13_375]
			fistp[tmp]
		};

		sprintf_s(txt, 100, "%d,", tmp);
		fputs(txt, c);
		w++;
		if (w % 16 == 0)
			fputs("\n\t", c);
	}

	fputs("};\n", c);
	//fputs(minmax1, c);
	//fputs(asserttxt1, c);
	fprintf(c, "\tc -= %d;\n", mint1);
	fputs("\treturn table[c];\n", c);
	fputs("}\n", c);

	fputs("#endif\n", c);

	fclose(c);
#endif
}
