#include "config.h"

#ifdef USE_DUMP
#include <windows.h>

#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <time.h>

#include "diag.h"
#include "ufmodint.h"

static dumptype dumpt = DUMPT_CONSOLE;
static FILE *df = NULL;
void dumpset(dumptype t)
{
	if (df)
	{
		fclose(df);
		df = NULL;
	}
	dumpt = t;
}

static void ensurefile(void)
{
	if (df) return;
	static int i = 1;
	//time_t t = time(NULL);
	char tmp[1024];
	//struct tm lt;
	//localtime_s(&lt, &t);
	//sprintf_s(tmp, 1024, "dump-%04d-%02d-%02dT%02d_%02d_%02d_%d.txt", lt.tm_year + 1900, lt.tm_mon + 1, lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec, i);
	sprintf_s(tmp, 1024, "dump-%05d.txt", i);
	fopen_s(&df, tmp, "w");
	assert(df != NULL);

	char tmp2[1024];
	sprintf_s(tmp2, 1024, "fc ufmodc\\dump-%05d.txt ufmod\\%s\n", i, tmp);
	OutputDebugString(tmp2);

	i++;
}

static int in = 0;
static const char *indents = "\t\t\t\t\t\t\t\t\t\t";
static void indent()
{
	if (in < 9)
		in++;
}
static void outdent()
{
	if (in > 0)
		in--;
}
static void dump(const char *fmt, ...)
{
	static char tmp[1000];
	va_list ap;
	va_start(ap, fmt);
	vsprintf_s(tmp, 1000, fmt, ap);
	va_end(ap);

	if (dumpt == DUMPT_CONSOLE)
	{
		OutputDebugString(indents + 10 - in);
		OutputDebugString(tmp);
		OutputDebugString("\n");
	}
	else if (dumpt == DUMPT_FILE)
	{
		ensurefile();
		fputs(indents + 10 - in, df);
		fputs(tmp, df);
		fputc('\n', df);
	}
}

static void dumpnote(int i, uF_NOTE *note);
static void dumppat(int i, int num_channels, uF_PAT *pat);

static void dump_samp(int i, int s, uF_SAMP *samp)
{
	dump("sample %d", i);

	indent();

	dump("%u %s", samp->_length, "sample length");
	dump("%u %s", samp->loopstart, "loop start");
	dump("%u %s", samp->looplen, "loop length");
	dump("%u %s", (unsigned int)samp->defvol, "default volume");
	dump("%d %s", (int)samp->finetune, "finetune value from - 128 to 127");
	dump("%02x %s", (unsigned int)samp->bytes, "type[b 0 - 1] : 0 - no loop 1 - forward loop 2 - bidirectional loop(aka ping - pong) [b 4] : 0 - 8 - bit sample data 1 - 16 - bit sample data");
	dump("%u %s", (unsigned int)samp->defpan, "default pan value from 0 to 255");
	dump("%d %s", (int)samp->relative, "relative note(signed value)");
	dump("%u %s", (unsigned int)samp->Resved, "reserved, samp->known values : 00h - regular delta packed sample data, ADh - ModPlug 4 - bit ADPCM packed sample data");
	dump("%u %s", (unsigned int)samp->loopmode, "loopmode");
	//dump("%u %s", (unsigned int)samp->_align, "align");

	/*
	dump("sample data");
	indent();
	unsigned short *data = &samp->buff;
	unsigned int len = samp->_length/32;
	char tmp[512];
	int s, k=32;
	for (int j = 0; j < 2 && k; j++)
	{
		while (len--)
		{
			s = k;
			char *t = tmp;
			while (s--)
				t += sprintf_s(t, 6, "%04X ", (unsigned int)*data++);
			dump(tmp);
		}
		len = 1;
		k = samp->_length % 32;
	}
	outdent();
	*/

	{
		static PCMWAVEFORMAT pcm =
		{
			WAVE_FORMAT_PCM,
			2,//channels
			FSOUND_MixRate,
			FSOUND_MixRate * 4,
			(2 * 16) / 8,//channels * bps / 8
			16//bps
		};

		char tmp[1024];
		unsigned int length;

		sprintf_s(tmp, 1024, "sample_%d_%d.wav", i, s);
		FILE *f;
		if (!fopen_s(&f, tmp, "wb"))
		{
			fwrite("RIFF", 4, 1, f);
			length = samp->_length*2 + 8 + sizeof(pcm) + 8;
			fwrite(&length, 4, 1, f);
			fwrite("WAVE", 4, 1, f);

			fwrite("fmt ", 4, 1, f);
			length = sizeof(pcm);
			fwrite(&length, 4, 1, f);
			fwrite(&pcm, sizeof(pcm), 1, f);

			length = samp->_length * 2;
			fwrite("data", 4, 1, f);
			fwrite(&length, 4, 1, f);
			fwrite(&samp->buff, samp->_length, 2, f);

			fclose(f);
		}
	}

	outdent();
}

static void dump_inst(int i, uF_INST *inst)
{
	char tmp[1024], *t;

	dump("instrument %d", i);

	indent();

	dump("keymap - sample keymap assignments");
	indent();
	t = tmp;
	for (int j = 0; j < 48; j++)
		t += sprintf_s(t, 6, "%02X ", (unsigned int)inst->keymap[j]);
	dump("%s", tmp);
	t = tmp;
	for (int j = 48; j < 96; j++)
		t += sprintf_s(t, 6, "%02X ", (unsigned int)inst->keymap[j]);
	dump("%s", tmp);
	outdent();

	dump("VOLPoints - volume envelope points");
	indent();
	t = tmp;
	for (int j = 0; j < 24; j++)
		t += sprintf_s(t, 6, "%04X ", (unsigned int)inst->VOLPoints[j]);
	dump("%s", tmp);
	outdent();

	dump("PANPoints - panning envelope points");
	indent();
	t = tmp;
	for (int j = 0; j < 24; j++)
		t += sprintf_s(t, 6, "%04X ", (unsigned int)inst->PANPoints[j]);
	dump("%s", tmp);
	outdent();

	dump("%u %s", (unsigned int)inst->VOLnumpoints, "number of volume envelope points");
	dump("%u %s", (unsigned int)inst->PANnumpoints, "number of panning envelope points");
	dump("%u %s", (unsigned int)inst->VOLsustain, "volume sustain point");
	dump("%u %s", (unsigned int)inst->VOLLoopStart, "volume envelope loop start");
	dump("%u %s", (unsigned int)inst->VOLLoopEnd, "volume envelope loop end");
	dump("%u %s", (unsigned int)inst->PANsustain, "panning sustain point");
	dump("%u %s", (unsigned int)inst->PANLoopStart, "panning envelope loop start");
	dump("%u %s", (unsigned int)inst->PANLoopEnd, "panning envelope loop end");
	dump("%u %s", (unsigned int)inst->VOLtype, "type of envelope, bit 0:On 1:Sustain 2:Loop");
	dump("%u %s", (unsigned int)inst->PANtype, "type of envelope, bit 0:On 1:Sustain 2:Loop");
	dump("%u %s", (unsigned int)inst->VIBtype, "instrument vibrato type");
	dump("%u %s", (unsigned int)inst->VIBsweep, "time it takes for vibrato to fully kick in");
	dump("%u %s", (unsigned int)inst->iVIBdepth, "depth of vibrato");
	dump("%u %s", (unsigned int)inst->VIBrate, "rate of vibrato");
	dump("%u %s", (unsigned int)inst->VOLfade, "fade out value");

	for (int s = 0; s < 16 && inst->sample[s] != NULL; s++)
		dump_samp(i, s, inst->sample[s]);

	outdent();
}

void dumpmodule(uF_MOD_STATE *_mod)
{
	uF_MOD *mod = &_mod->module;
	uF_INST *inst = mod->instrument;

	for (int i = 0; i < mod->numinsts; i++)
		dump_inst(i, inst++);

	dump("orders");
	indent();
	for (int i = 0; i < mod->numorders; i++)
		dump("%u %s", (unsigned int)mod->orderlist[i], "pattern playing order list");
	outdent();
	for (int i = 0; i < mod->numpatternsmem; i++)
		dumppat(i, mod->numchannels, &mod->pattern[i]);
}

static void dumpfchan(int i, FSOUND_CHANNEL *chan)
{
	dump("channel (FSOUND_CHANNEL) %d", i);
	indent();

	dump("%u %s", chan->actualvolume, "volume");
	dump("%u %s", chan->actualpan, "panning value");
	dump("%u %s", chan->fsampleoffset, "sample offset(sample starts playing from here)");
	dump("%u %s", chan->leftvolume, "adjusted volume for left channel(panning involved)");
	dump("%u %s", chan->rightvolume, "adjusted volume for right channel(panning involved)");
	dump("%u %s", chan->mixpos, "high part of 32:32 fractional position in sample");
	dump("%u %s", chan->speedlo, "playback rate - low part fractional");
	dump("%d %s", chan->speedhi, "playback rate - high part fractional");
	dump("%u %s", chan->ramp_LR_target, "ramp_LR_target");
	dump("%d %s", chan->ramp_leftspeed, "ramp_leftspeed");
	dump("%d %s", chan->ramp_rightspeed, "ramp_rightspeed");
	//dump("%p %s", chan->fsptr, "pointer to FSOUND_SAMPLE currently playing sample");
	dump("%u %s", chan->mixposlo, "low part of 32:32 fractional position in sample");
	dump("%u %s", chan->ramp_leftvolume, "ramp_leftvolume");
	dump("%u %s", chan->ramp_rightvolume, "ramp_rightvolume");
	dump("%u %s", (unsigned int)chan->ramp_count, "ramp_count");
	dump("%u %s", (unsigned int)chan->speeddir, "playback direction - 0 forwards or 1 backwards");

	outdent();
}

static void dumpchan(uF_MOD *module, int i, uF_CHAN *chan)
{
	dump("channel (uF_CHAN) %d", i);
	indent();

	dump("%u %s", (unsigned int)chan->note, "last note set in channel");
	dump("%u %s", (unsigned int)chan->samp, "last sample set in channel");
	dump("%u %s", (unsigned int)chan->notectrl, "flags for DoFlags proc");
	dump("%u %s", (unsigned int)chan->inst, "last instrument set in channel");
	//dump("%p %s", chan->cptr, "pointer to FSOUND_CHANNEL system mixing channel");
	dump("%d %s", chan->cptr-module->Channels, "number of FSOUND_CHANNEL system mixing channel");
	dump("%u %s", chan->freq, "current mod frequency period for this channel");
	dump("%d %s", chan->volume, "current mod volume for this channel");
	dump("%d %s", chan->voldelta, "delta for volume commands : tremolo / tremor, etc");
	dump("%d %s", chan->freqdelta, "delta for frequency commands : vibrato / arpeggio, etc");
	dump("%u %s", chan->pan, "current mod pan for this channel");

	dump("%u %s", chan->envvol.tick, "tick counter for envelope position volume");
	dump("%u %s", chan->envvol.pos, "envelope position volume");
	dump("%u %s", chan->envvol.delta, "delta step between points volume");

	dump("%u %s", chan->envpan.tick, "tick counter for envelope position pan");
	dump("%u %s", chan->envpan.pos, "envelope position pan");
	dump("%u %s", chan->envpan.delta, "delta step between points pan");

	dump("%u %s", chan->ivibsweeppos, "instrument vibrato sweep position");
	dump("%u %s", chan->ivibpos, "instrument vibrato position");
	dump("%u %s", (unsigned int)chan->keyoff, "flag whether keyoff has been hit or not");
	dump("%u %s", (unsigned int)chan->envvolstopped, "flag to say whether envelope has finished or not");
	dump("%u %s", (unsigned int)chan->envpanstopped, "flag to say whether envelope has finished or not");

	dump("%d %s", chan->envvolfrac.frac, "fractional interpolated envelope volume");
	dump("%u %s", chan->envvolfrac.val, "final interpolated envelope volume");

	dump("%d %s", chan->fadeoutvol, "volume fade out");

	dump("%d %s", chan->envpanfrac.frac, "fractional interpolated envelope pan");
	dump("%u %s", chan->envpanfrac.val, "final interpolated envelope pan");

	dump("%u %s", chan->period, "last period set in channel");
	dump("%u %s", chan->sampleoffset, "sample offset for this channel in SAMPLES");
	dump("%u %s", chan->portatarget, "note to porta to");
	dump("%u %s", chan->patloopno, "pattern loop variables for effect E6x");
	dump("%u %s", chan->patlooprow, "patlooprow");
	dump("%u %s", (unsigned int)chan->realnote, "last realnote set in channel");
	dump("%u %s", (unsigned int)chan->recenteffect, "previous row's effect. used to correct tremolo volume");
	dump("%d %s", (int)chan->portaupdown, "last porta up / down value");
	dump("%u %s", (unsigned int)chan->unused, "unused");
	dump("%u %s", (unsigned int)chan->xtraportadown, "last porta down value");
	dump("%u %s", (unsigned int)chan->xtraportaup, "last porta up value");
	dump("%u %s", (unsigned int)chan->volslide, "last volume slide value");
	dump("%u %s", (unsigned int)chan->panslide, "pan slide parameter");
	dump("%u %s", (unsigned int)chan->retrigx, "last retrig volume slide used");
	dump("%u %s", (unsigned int)chan->retrigy, "last retrig tick count used");
	dump("%u %s", (unsigned int)chan->portaspeed, "porta speed");
	dump("%u %s", (unsigned int)chan->vibpos, "vibrato position");
	dump("%u %s", (unsigned int)chan->vibspeed, "vibrato speed");
	dump("%d %s", (int)chan->vibdepth, "vibrato depth");
	dump("%u %s", (unsigned int)chan->tremolopos, "tremolo position");
	dump("%u %s", (unsigned int)chan->tremolospeed, "tremolo speed");
	dump("%d %s", (int)chan->tremolodepth, "tremolo depth");
	dump("%u %s", (unsigned int)chan->tremorpos, "tremor position");
	dump("%u %s", (unsigned int)chan->tremoron, "remembered parameters for tremor");
	dump("%u %s", (unsigned int)chan->tremoroff, "remembered parameters for tremor");
	dump("%u %s", (unsigned int)chan->wavecontrol, "waveform type for vibrato and tremolo (4bits each)");
	dump("%d %s", (int)chan->finevslup, "parameter for fine volume slide down");
	dump("%u %s", (unsigned int)chan->fineportaup, "parameter for fine porta slide up");
	dump("%u %s", (unsigned int)chan->fineportadown, "parameter for fine porta slide down");

	outdent();
}

static void dumpnote(int i, uF_NOTE *note)
{
	dump("%u %s", (unsigned int)note->unote, "note to play at(0 - 97) 97 = keyoff");
	dump("%u %s", (unsigned int)note->number, "sample being played(0 - 128)");
	dump("%u %s", (unsigned int)note->uvolume, "volume column value(0 - 64)  255 = no volume");
	dump("%u %s", (unsigned int)note->effect, "effect number(0 - 1Ah)");
	dump("%u %s", (unsigned int)note->eparam, "effect parameter(0 - 255)");
}

static void dumppat(int i, int num_channels, uF_PAT *pat)
{
	dump("pattern %d", i);
	indent();

	dump("%d %s", (int)pat->rows, "rows");
	dump("%u %s", (unsigned int)pat->patternsize, "patternsize");

	dump("notes");
	indent();
	//for (int j = 0; j < num_channels * pat->rows; j++)
	//	dumpnote(j, &pat->data[j]);

	char tmp[5][1000];
	char *t[5];
	uF_NOTE *note = pat->data;

	if (note == NULL)
	{
		dump("Pattern data is NULL!");
		outdent();
		outdent();
		return;
	}

	for (int r = 0; r < pat->rows; r++)
	{
		for (int f = 0; f < 5; f++)
			t[f] = &tmp[f][0];
		unsigned int cnt = 0;
		for (int c = 0; c < num_channels; c++)
		{
			t[0] += sprintf_s(t[0], 5, "%3d ", (unsigned int)note->unote);
			t[1] += sprintf_s(t[1], 5, "%3d ", (unsigned int)note->number);
			t[2] += sprintf_s(t[2], 5, "%3d ", (unsigned int)note->uvolume);
			t[3] += sprintf_s(t[3], 5, " %02X ", (unsigned int)note->effect);
			t[4] += sprintf_s(t[4], 5, "%3d ", (unsigned int)note->eparam);
			cnt += (unsigned int)note->unote + note->number + note->uvolume + note->effect + note->eparam;
			note++;
		}
		if (cnt != 0)
		{
			sprintf_s(t[0], 20, "unote");
			sprintf_s(t[1], 20, "number");
			sprintf_s(t[2], 20, "uvolume");
			sprintf_s(t[3], 20, "effect");
			sprintf_s(t[4], 20, "eparam\n");
			for (int f = 0; f < 5; f++)
				dump("%s", &tmp[f]);
		}
		else
		{
			dump("empty row");
		}
	}
	outdent();

	outdent();
}

static void dumpufmod(uF_MOD *mod)
{
	//dump("%p %s", mod->pattern, "pointer to FMUSIC_PATTERN array for this song");
	//dump("%p %s", mod->instrument, "pointer to FMUSIC_INSTRUMENT array for this song");

	dump("%u %s", mod->mixer_samplesleft, "");;
	dump("%u %s", mod->globalvolume, "global mod volume");
	dump("%u %s", mod->tick, "current mod tick");
	dump("%u %s", mod->speed, "speed of song in ticks per row");
	dump("%d %s", mod->order, "current song order position");
	dump("%d %s", mod->row, "current row in pattern");
	dump("%u %s", mod->patterndelay, "pattern delay counter");
	dump("%d %s", mod->nextorder, "current song order position");
	dump("%d %s", mod->nextrow, "current row in pattern");
	//dump("%u %s", mod->unused1, "unused1");
	dump("%u %s", mod->numchannels, "number of channels");
	//dump("%p %s", mod->Channels, "channel pool");
	//dump("%p %s", mod->uFMOD_Ch, "channel array for this song");

	dump("%u %s", mod->mixer_samplespertick, "mixer_samplespertick");
	dump("%u %s", mod->header_size, "header_size");

	dump("%d %s", (int)mod->numorders, "number of orders(song length)");
	dump("%u %s", (unsigned int)mod->restart, "restart position");
	dump("%u %s", (unsigned int)mod->numchannels_xm, "numchannels_xm");
	dump("%u %s", (unsigned int)mod->globalvsl, "global mod volume // numchannels_xm is usually a word, this has been jammed in between since numchannels is <= 32");
	dump("%u %s", (unsigned int)mod->numpatternsmem, "number of allocated patterns");
	dump("%u %s", (unsigned int)mod->numinsts, "number of instruments");
	dump("%u %s", (unsigned int)mod->flags, "flags such as linear frequency, format specific quirks, etc");
	dump("%u %s", (unsigned int)mod->defaultspeed, "defaultspeed");
	dump("%u %s", (unsigned int)mod->defaultbpm, "defaultbpm");

}

void dumpmod(uF_MOD_STATE *_mod)
{
	dumpufmod(&_mod->module);

	//for (int i = 0; i < 3; i++)
	//	dump("%u %s", _mod->mmt[i], "mmt");

	dump("%p %s", _mod->hHeap, "Heap");
	dump("%p %s", _mod->hThread, "Thread");
	dump("%p %s", _mod->hWaveOut, "hWaveOut");
	dump("%u %s", _mod->uFMOD_FillBlk, "uFMOD_FillBlk");
	dump("%p %s", _mod->SW_Exit, "file ptr");

	//for (int i = 0; i < FSOUND_BlockSize*2; i++)
	//	dump("%u", _mod->MixBuf[i]);

	dump("%u %s", (unsigned int)_mod->ufmod_noloop, "enable/disable restart loop");
	dump("%u %s", (unsigned int)_mod->ufmod_pause, "pause/resume");
	for (int i = 0; i < 2; i++)
		dump("%u %s", (unsigned int)_mod->mix_endflag[i], "mix_endflag");

	dump("%u %s", _mod->mmf_module_size, "module size");
	dump("%u %s", _mod->mmf_module_posn, "module position");
	dump("%p %s", _mod->mmf_module_address, "module address/resource");
	dump("%u %s", _mod->mmf_pad, "mmf pad");

	dump("%u %s", _mod->ufmod_vol, "global volume scale");

	dump("%p %s", _mod->uFMOD_fopen, "uFMOD_fopen");
	dump("%p %s", _mod->uFMOD_fread, "uFMOD_fread");
	dump("%p %s", _mod->uFMOD_fclose, "uFMOD_fclose");
	dump("%u %s", _mod->unused, "unused");

	//for (int i = 0; i < FSOUND_BufferSize; i++)
	//	dump("%u", _mod->databuf[i];

	dump("%p %s", _mod->MixBlock.lpData, "pointer to locked data buffer");
	dump("%u %s", _mod->MixBlock.dwBufferLength, "length of data buffer");
	dump("%u %s", _mod->MixBlock.dwBytesRecorded, "used for input only");
	dump("%p %s", _mod->MixBlock.dwUser, "for client's use");
	dump("%u %s", _mod->MixBlock.dwFlags, "assorted flags (see defines)");
	dump("%u %s", _mod->MixBlock.dwLoops, "loop control counter");
	dump("%p %s", _mod->MixBlock.lpNext, "reserved for driver");
	dump("%p %s", _mod->MixBlock.reserved, "reserved for driver");

	for (int i = 0; i < 4; i++)
		dump("%u %s", (unsigned int)_mod->RealBlock[i], "RealBlock");

	dump("%u %s", _mod->time_ms, "time_ms");

	//for (int i = 0; i < totalblocks; i++)
	//	dump("%u %s", _mod->uF_STATS tInfo[i];

	dump("%s %s", _mod->szTtl, "track title");

	//dump("%u %s", _mod.uF_SAMP DummySamp;
}

void dumpstate(const char *when, uF_MOD_STATE *_mod)
{
	dump("%s", when);
	indent();

	dumpufmod(&_mod->module);

	uF_MOD *mod = &_mod->module;
	FSOUND_CHANNEL *fchan = mod->Channels;
	uF_CHAN *chan = mod->uFMOD_Ch;

	for (int i = 0; i < (int)mod->numchannels*2; i++)
		dumpfchan(i, fchan++);

	for (int i = 0; i < (int)mod->numchannels; i++)
		dumpchan(&_mod->module, i, chan++);

	outdent();
}

static void dumpb(unsigned char *data, unsigned int count)
{
	const int perrow = 32;
	unsigned int len = count / perrow;
	char tmp[512];
	int s, k = perrow;
	for (int j = 0; j < 2 && k; j++)
	{
		while (len--)
		{
			s = k;
			char *t = tmp;
			while (s--)
				t += sprintf_s(t, 4, "%02X ", (unsigned int)*data++);
			dump(tmp);
		}
		len = 1;
		k = count % perrow;
	}
}

static void dumpw(unsigned short *data, unsigned int count)
{
	const int perrow = 32;
	unsigned int len = count / perrow;
	char tmp[512];
	int s, k = perrow;
	for (int j = 0; j < 2 && k; j++)
	{
		while (len--)
		{
			s = k;
			char *t = tmp;
			while (s--)
				t += sprintf_s(t, 6, "%04X ", (unsigned int)*data++);
			dump(tmp);
		}
		len = 1;
		k = count % perrow;
	}
}

static void dumpl(unsigned int *data, unsigned int count)
{
	const int perrow = 16;
	unsigned int len = count / perrow;
	char tmp[512];
	int s, k = perrow;
	for (int j = 0; j < 2 && k; j++)
	{
		while (len--)
		{
			s = k;
			char *t = tmp;
			while (s--)
				t += sprintf_s(t, 10, "%08X ", *data++);
			dump(tmp);
		}
		len = 1;
		k = count % perrow;
	}
}

void dumpbuffer(const char *name, void *ptr, unsigned int count, unsigned char bits)
{
	dump("%s", name);
	indent();
	switch (bits)
	{
		case 8: dumpb((unsigned char *)ptr, count); break;
		case 16: dumpw((unsigned short *)ptr, count); break;
		case 32: dumpl((unsigned int *)ptr, count); break;
	}
	outdent();
}
#endif
