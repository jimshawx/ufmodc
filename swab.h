#include "config.h"

#ifdef USE_SWAB
#include "ufmodint.h"
void swabufxm(uF_XM *xm);
void swabufmod(uF_MOD *mod);
void swabufinsthdr(uF_INSTRUMENT_HEADER *insthdr);
void swabufinst(uF_INST *inst);
void swabufsamphdr(uF_SAMPLE_HEADER *samphdr);
void swabufpathdr(uF_PATTERN_HEADER *pathdr);
void swabsample(unsigned short *smp, unsigned int count);
#else
#define swabufxm(x)
#define swabufmod(x)
#define swabufinsthdr(x)
#define swabufinst(x)
#define swabufsamphdr(x)
#define swabufpathdr(x)
#define swabsample(x,y)
#endif
