#ifndef NDEBUG
#define USE_DUMP
#define USE_MEMCHECK
#define USE_TABLE_HZ
#endif

#ifdef _WIN64
//#define USE_TABLE_HZ
#else
#define INLINE_X86
#endif

#define USE_FILE_MAPPING

//#define USE_SWAB

//#define INTEGRATION_TEST
