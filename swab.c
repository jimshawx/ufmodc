#include "config.h"

#ifdef USE_SWAB

#include "ufmodint.h"

static void swabw(unsigned short *s) { *s = (*s >> 8) | (*s << 8); }
static void swabl(unsigned int *s) { *s = (*s >> 24) | (*s << 24) | ((*s<<8)&0xff0000) | ((*s>>8)&0xff00); }

void swabufxm(uF_XM *xm)
{
	swabw(&xm->version);
}

void swabufmod(uF_MOD *mod)
{
	swabl(&mod->header_size);
	swabw(&mod->numorders);
	swabw(&mod->restart);
	swabw(&mod->numpatternsmem);
	swabw(&mod->numinsts);
	swabw(&mod->flags);
	swabw(&mod->defaultspeed);
	swabw(&mod->defaultbpm);
}

void swabufinsthdr(uF_INSTRUMENT_HEADER *insthdr)
{
	swabl(&insthdr->headersize);
	swabw(&insthdr->samplecount);
	swabl(&insthdr->sampleheadersize);
}

void swabufinst(uF_INST *inst)
{
	for (int i = 0; i < 24; i++)
	{
		swabw(&inst->VOLPoints[i]);
		swabw(&inst->PANPoints[i]);
	}
	swabw(&inst->VOLfade);
}

void swabufsamphdr(uF_SAMPLE_HEADER *samphdr)
{
	swabl(&samphdr->loadxm_sample_2);
	swabl(&samphdr->loadxm_s0loopstart);
	swabl(&samphdr->loadxm_s0looplen);
}

void swabsample(unsigned short *smp, unsigned int count)
{
	while (count--)
		swabw(*smp++);
}

void swabufpathdr(uF_PATTERN_HEADER *pathdr)
{
	swabl(&pathdr->headersize);
	swabw(&pathdr->rowcount);
	swabw(&pathdr->loadxm_pat_size);
}

#endif
